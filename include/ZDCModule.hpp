#pragma once

#include "StatusBits.hpp"

#include <algorithm>
#include <array>
#include <stdexcept>
#include <functional>
#include <bitset>

#include "ROOT/RVec.hxx"

namespace side {
  unsigned int constexpr C = 0;
  unsigned int constexpr A = 1;
} // namespace side
std::array<unsigned int, 2> constexpr SIDES = {side::C, side::A};
std::array<char, 2> constexpr SIDE_LABELS = {'C', 'A'};

inline void checkSide(unsigned int const side) {
  if (side != 0 && side != 1) {
    throw std::runtime_error("invalid side: " + std::to_string(side));
  }
}

inline char getSideLabel(unsigned int const side) {
  checkSide(side);
  return SIDE_LABELS.at(side);
}

unsigned int constexpr N_RPD_CHANNELS = 16;

unsigned int constexpr N_RECO_MODULES = 4;
enum struct RecoModule : unsigned int {
  EM = 0, HAD1 = 1, HAD2 = 2, HAD3 = 3
};

unsigned int constexpr N_SIM_MODULES = 7;
unsigned int constexpr N_SIM_MODULES_USED = 6;
enum struct SimModule : unsigned int {
  EM = 0, HAD1 = 1, HAD2 = 2, HAD3 = 3, RPD = 4, BRAN = 5
};

std::array<std::string, N_SIM_MODULES_USED> const SIM_MODULE_NAMES = {"EM", "HAD1", "HAD2", "HAD3", "RPD", "BRAN"};
std::array<std::string, N_RECO_MODULES> const RECO_MODULE_NAMES = {"EM", "HAD1", "HAD2", "HAD3"};

// plot in order of location in detector, closest to beam first
std::array<unsigned int, N_SIM_MODULES_USED> constexpr MODULE_PLOT_ORDER = {
  static_cast<unsigned int>(SimModule::EM),
  static_cast<unsigned int>(SimModule::RPD),
  static_cast<unsigned int>(SimModule::BRAN),
  static_cast<unsigned int>(SimModule::HAD1),
  static_cast<unsigned int>(SimModule::HAD2),
  static_cast<unsigned int>(SimModule::HAD3),
};

inline std::vector<SimModule> getModulesBeforeInclusive(SimModule const mod) {
  std::vector<SimModule> modsBeforeInclusive;
  auto const modIndex = static_cast<unsigned int>(mod);
  auto const *it = std::find(MODULE_PLOT_ORDER.begin(), MODULE_PLOT_ORDER.end(), modIndex);
  if (it == MODULE_PLOT_ORDER.end()) {
    throw std::runtime_error("unable to locate sim module " + std::to_string(modIndex) + " in plot order");
  }
  auto const modPlotOrderIndex = it - MODULE_PLOT_ORDER.begin();
  for (unsigned int plotOrderIndex = 0; plotOrderIndex <= modPlotOrderIndex; plotOrderIndex++) {
    modsBeforeInclusive.push_back(static_cast<SimModule>(MODULE_PLOT_ORDER.at(plotOrderIndex)));
  }
  return modsBeforeInclusive;
}

inline unsigned int getIndex(unsigned int const side, unsigned int const index, unsigned int const sideSize) {
  checkSide(side);
  return side*sideSize + index;
};
inline unsigned int getZDCModuleIndex(unsigned int const side, RecoModule const mod) {
  return getIndex(side, static_cast<unsigned int>(mod), N_RECO_MODULES);
};
inline unsigned int getZDCModuleIndex(unsigned int const side, SimModule const mod) {
  return getIndex(side, static_cast<unsigned int>(mod), N_SIM_MODULES);
};
inline unsigned int getRPDChannelIndex(unsigned int const side, unsigned int const ch) {
  return getIndex(side, ch, N_RPD_CHANNELS);
};

namespace geometry {
  /*
  we use a simple convention for channels and rows
  WARNING: this is different from the reconstruction, which uses bottom row = 0!

  col ->    0    1    2    3
        ---------------------
  row 0  |  0 |  1 |  2 |  3 |
        ---------------------
  row 1  |  4 |  5 |  6 |  7 |
        ---------------------
  row 2  |  8 |  9 | 10 | 11 |
        ---------------------
  row 3  | 12 | 13 | 14 | 15 |
        ---------------------

  and RPD coordinates (origin at center of RPD):

  +y
    ^
    |
    o---> +x

  */

  double constexpr RPD_TILE_WIDTH = 11.4; // mm
  unsigned int constexpr N_ROWS = 4;
  unsigned int constexpr N_COLS = 4;

  inline unsigned int getNoNonsenseRow(unsigned int const ch) {
    return ch / N_COLS;
  }

  inline unsigned int getCol(unsigned int const ch) {
    return ch % N_COLS;
  }

  inline unsigned int getChannel(unsigned int const row, unsigned int const col) {
    return N_COLS*row + col;
  }

  /**
  * @brief get the vertical extent of rods of channels in the provided row
  * 
  * @param row 
  * @param useSquareActiveArea true if the active area is simply region enclosing tiles, false if active area extends 20 mm above tiles
  * @return std::array<double, 2> {lower vertical bound, upper vertical bound}
  */
  inline std::array<double, 2> getRowRodYBounds(unsigned int const row, bool const useSquareActiveArea) {
    if (useSquareActiveArea) {
      return {(1 - static_cast<int>(row))*RPD_TILE_WIDTH, 2*RPD_TILE_WIDTH};
    } else {
      return {(1 - static_cast<int>(row))*RPD_TILE_WIDTH, 2*RPD_TILE_WIDTH + 20};
    }
  }

  /**
  * @brief get the horizontal extent of rods of channels in the provided column
  * 
  * @return std::array<double, 2> {lower horizontal bound, upper horizontal bound}
  */
  inline std::array<double, 2> getColRodXBounds(unsigned int const col, unsigned int const side) {
    std::array<double, 2> xBounds = {(static_cast<int>(col) - 2)*RPD_TILE_WIDTH, (static_cast<int>(col) - 1)*RPD_TILE_WIDTH};
    if (side == side::A) {
      xBounds.at(0) *= -1;
      xBounds.at(1) *= -1;
    }
    return xBounds;
  }
} // namespace geometry

namespace alignment {
  struct RPDPos {
    float x;
    float y;
    float z;
  };
  std::array<RPDPos, 2> constexpr RUN3_RPD_POSITION = {{
    {-2.012, 21.388, -141489.5},
    {1.774, 21.344, 141459.0},
  }};
} // namespace alignment

std::string const TRUTH_TOTAL_ENERGY_BRANCH = "zdc_ZdcTruthTotal";
std::string const MODULE_TRUTH_TOTAL_ENERGIES_BRANCH = "zdc_ZdcModuleTruthTotal";

unsigned int constexpr N_TRUTH_ENERGY_TYPES = 5;
// see https://gitlab.cern.ch/atlas/athena/-/blob/main/Calorimeter/CaloSimEvent/CaloSimEvent/CaloCalibrationHit.h
// see https://gitlab.cern.ch/atlas/athena/-/blob/main/Calorimeter/CaloG4Sim/src/SimulationEnergies.cc
enum struct TruthEnergyType : unsigned int {
  Total = 0, // all energy, including invisible and escaped (Invisible + EM + NonEM + Escaped)
  Invisible = 1, // "invisible energy loss (usually nuclear binding energy)""
  EM = 2, // "energy loss by EM processes"
  NonEM = 3, // "visible energy loss by non EM processes (pion dEdx for instance)"
  Escaped = 4, // "energy which escaped from this cell because of production of neutrino (or escaping muon energy)"
};
std::array<std::string, N_TRUTH_ENERGY_TYPES> const TRUTH_ENERGY_TYPE_NAMES = {
  "Total", "Invisible", "EM", "NonEM", "Escaped"
};
std::array<std::string, N_TRUTH_ENERGY_TYPES> const TRUTH_ENERGY_BRANCHES = {
  "zdc_ZdcTruthTotal",
  "zdc_ZdcTruthInvisible",
  "zdc_ZdcTruthEM",
  "zdc_ZdcTruthNonEM",
  "zdc_ZdcTruthEscaped",
};
std::array<std::string, N_TRUTH_ENERGY_TYPES> const MODULE_TRUTH_ENERGIES_BRANCHES = {
  "zdc_ZdcModuleTruthTotal",
  "zdc_ZdcModuleTruthInvisible",
  "zdc_ZdcModuleTruthEM",
  "zdc_ZdcModuleTruthNonEM",
  "zdc_ZdcModuleTruthEscaped",
};

std::string const MODULE_TRUTH_N_PHOTONS_BRANCH = "zdc_ZdcModuleTruthNphotons";
std::string const RPD_CHANNEL_TRUTH_N_PHOTONS_BRANCH = "zdc_RpdModuleTruthNphotons";

enum TruthParticleType : unsigned int {
  toSideC = side::C, // particle is heading towards side C
  toSideA = side::A, // particle is heading towards side A
  spurious, // not sure what this is, but it's not a neutron and shouldn't be in the TTree! >:(
};

struct TruthParticle {
  TruthParticleType type;
  float x0; // initial x in ATLAS coordinates
  float y0; // initial y in ATLAS coordinates
  float z0; // initial z in ATLAS coordinates
  float px0; // initial x momentum
  float py0; // initial y momentum
  float pz0; // initial z momentum
  float energy;
  float projectedX; // x position of particle path projected to face of RPD, in ATLAS coordinates
  float projectedY; // y position of particle path projected to face of RPD, in ATLAS coordinates
};

float constexpr SPURIOUS_TRUTH_PARTICLE_Z_MOMENTUM_THRESHOLD = 10000; // MeV

inline TruthParticleType classifyTruthParticle(float const pz) {
  if (std::abs(pz) > SPURIOUS_TRUTH_PARTICLE_Z_MOMENTUM_THRESHOLD) {
    if (pz < 0) {
      return TruthParticleType::toSideC;
    } else if (pz > 0) {
      return TruthParticleType::toSideA;
    }
  }
  return TruthParticleType::spurious;
}

inline std::pair<float, float> projectTruthParticle(unsigned int const toSide, float const x0, float const y0, float const z0, float const px, float const py, float const pz) {
  float const horizontalAngle = std::atan2(px, pz);
  float const verticalAngle = std::atan2(py, pz);
  auto const& pos = alignment::RUN3_RPD_POSITION.at(toSide);
  float const dz = pos.z - z0;
  float const xAtRPDATLAS = std::tan(horizontalAngle)*dz; // in ATLAS coordinates
  float const yAtRPDATLAS = std::tan(verticalAngle)*dz; // in ATLAS coordinates
  return {xAtRPDATLAS, yAtRPDATLAS};
}

namespace RDFHelp {
  /**
  * @brief get a function that picks an element at a specified index given a vector.
  */
  template <typename T>
  inline std::function<T(ROOT::VecOps::RVec<T> const& vec)> getVectorUnpackerFunc(unsigned int const index) {
    return [index] (ROOT::VecOps::RVec<T> const& vec) {
      return vec.at(index);
    };
  }
  template <typename T>
  inline std::function<T(ROOT::VecOps::RVec<T> const& vec)> getSide(unsigned int const side) {
    return getVectorUnpackerFunc<T>(side);
  }
  template <typename T>
  inline std::function<T(ROOT::VecOps::RVec<T> const& vec)> getZDCModule(unsigned int const side, SimModule const mod) {
    return getVectorUnpackerFunc<T>(getZDCModuleIndex(side, mod));
  }
  template <typename T>
  inline std::function<T(ROOT::VecOps::RVec<T> const& vec)> getZDCModule(unsigned int const side, RecoModule const mod) {
    return getVectorUnpackerFunc<T>(getZDCModuleIndex(side, mod));
  }
  template <typename T>
  inline std::function<T(ROOT::VecOps::RVec<T> const& vec)> getRPDChannel(unsigned int const side, unsigned int const ch) {
    return getVectorUnpackerFunc<T>(getRPDChannelIndex(side, ch));
  }

  namespace reco {
    namespace ZDC {
      inline std::function<bool(ROOT::VecOps::RVec<short> const& zdc_ZdcStatus)> checkValid(unsigned int const side) {
        // cast short to bool to check status
        return [side] (ROOT::VecOps::RVec<short> const& zdc_ZdcStatus) -> bool {
          return zdc_ZdcStatus.at(side);
        };
      }
    } // namespace ZDC
    namespace ZDCModule {
      inline std::function<bool(ROOT::VecOps::RVec<unsigned int> const& zdc_ZdcModuleStatus)> checkValid(unsigned int const side, RecoModule const mod) {
        return [side, mod] (ROOT::VecOps::RVec<unsigned int> const& zdc_ZdcModuleStatus) {
          return !std::bitset<status::ZDCModule::N_BITS> (zdc_ZdcModuleStatus.at(getZDCModuleIndex(side, mod)))[status::ZDCModule::FailBit];
        };
      }
    } // namespace ZDCModule
    namespace RPD {
      inline std::function<bool(ROOT::VecOps::RVec<unsigned int> const& zdc_RpdSideStatus)> checkValid(unsigned int const side) {
        return [side] (ROOT::VecOps::RVec<unsigned int> const& zdc_RpdSideStatus) {
          return std::bitset<status::RPD::N_BITS> (zdc_RpdSideStatus.at(side))[status::RPD::ValidBit];
        };
      }
      inline std::function<float(ROOT::VecOps::RVec<float> const& zdc_RpdChannelAmplitude)> getSumSumADC(unsigned int const side) {
        return [side] (ROOT::VecOps::RVec<float> const& zdc_RpdChannelAmplitude) {
          float sum = 0;
          for (unsigned int ch = 0; ch < N_RPD_CHANNELS; ch++) {
            sum += zdc_RpdChannelAmplitude.at(getRPDChannelIndex(side, ch));
          }
          return sum;
        };
      }
    } // namespace RPD
    namespace centroid {
      inline std::function<bool(ROOT::VecOps::RVec<unsigned int> const& zdc_centroidStatus)> checkValid(unsigned int const side) {
        return [side] (ROOT::VecOps::RVec<unsigned int> const& zdc_centroidStatus) {
          return std::bitset<status::Centroid::N_BITS> (zdc_centroidStatus.at(side))[status::Centroid::ValidBit];
        };
      }
    } // namespace centroid
  } // namespace reco
  namespace truth {
    namespace EMNonEM {
      namespace ZDC {
        std::string const outputColumn = "zdc_ZdcTruthEMNonEM";
        std::vector<std::string> const inputColumns {
          "zdc_ZdcTruthEM",
          "zdc_ZdcTruthNonEM",
        };
        inline ROOT::VecOps::RVec<float> calculate(
          ROOT::VecOps::RVec<float> const& zdc_ZdcTruthEM,
          ROOT::VecOps::RVec<float> const& zdc_ZdcTruthNonEM
        ) {
          std::vector<float> ZDCEnergy(2, 0);
          for (auto const side : SIDES) {
            ZDCEnergy.at(side) += zdc_ZdcTruthEM.at(side);
            ZDCEnergy.at(side) += zdc_ZdcTruthNonEM.at(side);
          }
          return ZDCEnergy;
        }
      } // namespace ZDC
      namespace ZDCModule {
        std::string const outputColumn = "zdc_ZdcModuleTruthEMNonEM";
        std::vector<std::string> const inputColumns {
          "zdc_ZdcModuleTruthEM",
          "zdc_ZdcModuleTruthNonEM",
        };
        inline ROOT::VecOps::RVec<float> calculate(
          ROOT::VecOps::RVec<float> const& zdc_ZdcModuleTruthEM,
          ROOT::VecOps::RVec<float> const& zdc_ZdcModuleTruthNonEM
        ) {
          std::vector<float> ZDCModuleEnergy(2*N_SIM_MODULES, 0);
          for (auto const side : SIDES) {
            for (unsigned int modIndex = 0; modIndex < N_SIM_MODULES; modIndex++) {
              auto const index = getIndex(side, modIndex, N_SIM_MODULES);
              ZDCModuleEnergy.at(index) += zdc_ZdcModuleTruthEM.at(index);
              ZDCModuleEnergy.at(index) += zdc_ZdcModuleTruthNonEM.at(index);
            }
          }
          return ZDCModuleEnergy;
        }
      } // namespace ZDCModule
    } // namespace EMNonEM
    namespace visible {
      namespace ZDC {
        std::string const outputColumn = "zdc_ZdcTruthVisible";
        std::vector<std::string> const inputColumns {
          "zdc_ZdcTruthEM",
          "zdc_ZdcTruthNonEM",
          "zdc_ZdcTruthEscaped",
        };
        inline ROOT::VecOps::RVec<float> calculate(
          ROOT::VecOps::RVec<float> const& zdc_ZdcTruthEM,
          ROOT::VecOps::RVec<float> const& zdc_ZdcTruthNonEM,
          ROOT::VecOps::RVec<float> const& zdc_ZdcTruthEscaped
        ) {
          std::vector<float> ZDCEnergy(2, 0);
          for (auto const side : SIDES) {
            ZDCEnergy.at(side) += zdc_ZdcTruthEM.at(side);
            ZDCEnergy.at(side) += zdc_ZdcTruthNonEM.at(side);
            ZDCEnergy.at(side) += zdc_ZdcTruthEscaped.at(side);
          }
          return ZDCEnergy;
        }
      } // namespace ZDC
      namespace ZDCModule {
        std::string const outputColumn = "zdc_ZdcModuleTruthVisible";
        std::vector<std::string> const inputColumns {
          "zdc_ZdcModuleTruthEM",
          "zdc_ZdcModuleTruthNonEM",
          "zdc_ZdcModuleTruthEscaped",
        };
        inline ROOT::VecOps::RVec<float> calculate(
          ROOT::VecOps::RVec<float> const& zdc_ZdcModuleTruthEM,
          ROOT::VecOps::RVec<float> const& zdc_ZdcModuleTruthNonEM,
          ROOT::VecOps::RVec<float> const& zdc_ZdcModuleTruthEscaped
        ) {
          std::vector<float> ZDCModuleEnergy(2*N_SIM_MODULES, 0);
          for (auto const side : SIDES) {
            for (unsigned int modIndex = 0; modIndex < N_SIM_MODULES; modIndex++) {
              auto const index = getIndex(side, modIndex, N_SIM_MODULES);
              ZDCModuleEnergy.at(index) += zdc_ZdcModuleTruthEM.at(index);
              ZDCModuleEnergy.at(index) += zdc_ZdcModuleTruthNonEM.at(index);
              ZDCModuleEnergy.at(index) += zdc_ZdcModuleTruthEscaped.at(index);
            }
          }
          return ZDCModuleEnergy;
        }
      } // namespace ZDCModule
    } // namespace visible

    std::vector<std::string> const getParticlesInput = {
      "zdc_ZdcTruthParticlePosx",
      "zdc_ZdcTruthParticlePosy",
      "zdc_ZdcTruthParticlePosz",
      "zdc_ZdcTruthParticlePx",
      "zdc_ZdcTruthParticlePy",
      "zdc_ZdcTruthParticlePz",
      "zdc_ZdcTruthParticleEnergy",
    };
    inline ROOT::VecOps::RVec<TruthParticle> getParticles(
      ROOT::VecOps::RVec<float> const& zdc_ZdcTruthParticlePosx,
      ROOT::VecOps::RVec<float> const& zdc_ZdcTruthParticlePosy,
      ROOT::VecOps::RVec<float> const& zdc_ZdcTruthParticlePosz,
      ROOT::VecOps::RVec<float> const& zdc_ZdcTruthParticlePx,
      ROOT::VecOps::RVec<float> const& zdc_ZdcTruthParticlePy,
      ROOT::VecOps::RVec<float> const& zdc_ZdcTruthParticlePz,
      ROOT::VecOps::RVec<float> const& zdc_ZdcTruthParticleEnergy
    ) {
      std::vector<TruthParticle> particles;
      for (unsigned int i = 0; i < zdc_ZdcTruthParticlePz.size(); i++) {
        auto const type = classifyTruthParticle(zdc_ZdcTruthParticlePz.at(i));
        if (type == TruthParticleType::spurious) continue;
        auto const [xAtRPDATLAS, yAtRPDATLAS] = projectTruthParticle(
          type,
          zdc_ZdcTruthParticlePosx.at(i),
          zdc_ZdcTruthParticlePosy.at(i),
          zdc_ZdcTruthParticlePosz.at(i),
          zdc_ZdcTruthParticlePx.at(i),
          zdc_ZdcTruthParticlePy.at(i),
          zdc_ZdcTruthParticlePz.at(i)
        );
        particles.push_back({
          type,
          zdc_ZdcTruthParticlePosx.at(i),
          zdc_ZdcTruthParticlePosy.at(i),
          zdc_ZdcTruthParticlePosz.at(i),
          zdc_ZdcTruthParticlePx.at(i),
          zdc_ZdcTruthParticlePy.at(i),
          zdc_ZdcTruthParticlePz.at(i),
          zdc_ZdcTruthParticleEnergy.at(i),
          xAtRPDATLAS,
          yAtRPDATLAS
        });
      }
      return particles;
    }

    inline float ATLASToRPDCoordinatesX(unsigned int const side, float const x) {
      return x - alignment::RUN3_RPD_POSITION.at(side).x;
    }
    inline float ATLASToRPDCoordinatesY(unsigned int const side, float const y) {
      return y - alignment::RUN3_RPD_POSITION.at(side).y;
    }

    inline std::function<ROOT::VecOps::RVec<float>(ROOT::VecOps::RVec<TruthParticle> const& truthParticles)> getAllSideParticleProjectedX(
      unsigned int const side, bool const RPDCoordinates
    ) {
      return [side, RPDCoordinates] (ROOT::VecOps::RVec<TruthParticle> const& truthParticles) {
        std::vector<float> truthParticleProjectedX;
        for (auto const& particle : truthParticles) {
          if (particle.type != side) continue;
          if (RPDCoordinates) {
            truthParticleProjectedX.push_back(ATLASToRPDCoordinatesX(side, particle.projectedX));
          } else {
            truthParticleProjectedX.push_back(particle.projectedX);
          }
        }
        if (truthParticleProjectedX.empty()) {
          throw std::runtime_error("no particles matching side " + std::to_string(side));
        }
        return truthParticleProjectedX;
      };
    }
    inline std::function<ROOT::VecOps::RVec<float>(ROOT::VecOps::RVec<TruthParticle> const& truthParticles)> getAllSideParticleProjectedY(
      unsigned int const side, bool const RPDCoordinates
    ) {
      return [side, RPDCoordinates] (ROOT::VecOps::RVec<TruthParticle> const& truthParticles) {
        std::vector<float> truthParticleProjectedY;
        for (auto const& particle : truthParticles) {
          if (particle.type != side) continue;
          if (RPDCoordinates) {
            truthParticleProjectedY.push_back(ATLASToRPDCoordinatesY(side, particle.projectedY));
          } else {
            truthParticleProjectedY.push_back(particle.projectedY);
          }
        }
        if (truthParticleProjectedY.empty()) {
          throw std::runtime_error("no particles matching side " + std::to_string(side));
        }
        return truthParticleProjectedY;
      };
    }
    inline std::function<ROOT::VecOps::RVec<float>(ROOT::VecOps::RVec<TruthParticle> const& truthParticles)> getAllSideParticleEnergies(unsigned int const side) {
      return [side] (ROOT::VecOps::RVec<TruthParticle> const& truthParticles) {
        std::vector<float> truthParticleEnergies;
        for (auto const& particle : truthParticles) {
          if (particle.type != side) continue;
          truthParticleEnergies.push_back(particle.energy);
        }
        if (truthParticleEnergies.empty()) {
          throw std::runtime_error("no particles matching side " + std::to_string(side));
        }
        return truthParticleEnergies;
      };
    }

    class TruthCentroid {
     public:
      TruthCentroid() = default;
      TruthCentroid(unsigned int const side, unsigned int const nParticles, float const xATLAS, float const yATLAS)
        : m_side(side), m_nParticles(nParticles), m_xATLAS(xATLAS), m_yATLAS(yATLAS) {}
      /**
      * @param RPDCoordinates true: use coordinates in which RPD active area is origin, false: use ATLAS coordinates
      */
      unsigned int const& getNParticles() const {
        return m_nParticles;
      }
      float getX(bool const RPDCoordinates) const {
        if (RPDCoordinates) {
          return ATLASToRPDCoordinatesX(m_side, m_xATLAS);
        }
        return m_xATLAS;
      }
      float getY(bool const RPDCoordinates) const {
        if (RPDCoordinates) {
          return ATLASToRPDCoordinatesY(m_side, m_yATLAS);
        }
        return m_yATLAS;
      }
     private:
      unsigned int m_side;
      unsigned int m_nParticles;
      float m_xATLAS;
      float m_yATLAS;
    };

    inline std::array<TruthCentroid, 2> getTruthCentroids(ROOT::VecOps::RVec<TruthParticle> const& particles) {
      std::array<TruthCentroid, 2> truthCentroids {};
      for (auto const side : SIDES) {
        unsigned int nParticles = 0;
        float x = 0;
        float y = 0;
        for (auto const& particle : particles) {
          if (particle.type != side) continue;
          nParticles++;
          x += particle.projectedX;
          y += particle.projectedY;
        }
        if (nParticles == 0) {
          throw std::runtime_error("no truth particles for side " + std::to_string(getSideLabel(side)) + "!");
        }
        x /= nParticles;
        y /= nParticles;
        truthCentroids.at(side) = {side, nParticles, x, y};
      }
      return truthCentroids;
    }

    inline std::function<unsigned int(std::array<TruthCentroid, 2> const& truthCentroids)> getSideNTruthParticles(unsigned int const side) {
      return [side] (std::array<TruthCentroid, 2> const& truthCentroids) {
        return truthCentroids.at(side).getNParticles();
      };
    }
    /**
     * @param RPDCoordinates true: use coordinates in which RPD active area is origin, false: use ATLAS coordinates
     */
    inline std::function<float(std::array<TruthCentroid, 2> const& truthCentroids)> getSideTruthXCentroid(unsigned int const side, bool const RPDCoordinates) {
      return [side, RPDCoordinates] (std::array<TruthCentroid, 2> const& truthCentroids) {
        return truthCentroids.at(side).getX(RPDCoordinates);
      };
    }
    /**
     * @param RPDCoordinates true: use coordinates in which RPD active area is origin, false: use ATLAS coordinates
     */
    inline std::function<float(std::array<TruthCentroid, 2> const& truthCentroids)> getSideTruthYCentroid(unsigned int const side, bool const RPDCoordinates) {
      return [side, RPDCoordinates] (std::array<TruthCentroid, 2> const& truthCentroids) {
        return truthCentroids.at(side).getY(RPDCoordinates);
      };
    }

  } // namespace truth
} // namespace RDFHelp
