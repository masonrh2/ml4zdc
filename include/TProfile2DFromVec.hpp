#pragma once

#include <optional>
#include <tuple>

#include "ROOT/RDataFrame.hxx"

/**
 * @brief unlike Histo2D, RDataFrame's TProfile2D does not (yet?) support use of vector columns
 * this class is a workaround for doing that
 *
 * example:
 * Book<ROOT::RVec<float>, ROOT::RVec<float>, ROOT::RVec<float>>(
 *   TProfile2DFromVec {{
 *     "name", "title", 100, 0, 10, 100, 0, 10
 *   }},
 *   {"x", "y", "z"}
 * )
 */

class TProfile2DFromVec : public ROOT::Detail::RDF::RActionImpl<TProfile2DFromVec> {
 public:
  /// This type is a requirement for every helper.
  using Result_t = TProfile2D;
 private:
  std::vector<std::shared_ptr<TProfile2D>> fProfiles; // one per data processing slot
  std::vector<unsigned int> nEvents; // one per data processing slot

 public:
  /// This constructor takes all the parameters necessary to build the THnTs. In addition, it requires the names of
  /// the columns which will be used.
  TProfile2DFromVec(
    ROOT::RDF::TProfile2DModel const& model
  ) {
    auto const nSlots = ROOT::IsImplicitMTEnabled() ? ROOT::GetThreadPoolSize() : 1;
    for (auto i : ROOT::TSeqU(nSlots)) {
        // sadly we can't construct the profile from the model directly...
        auto prof = std::make_shared<TProfile2D>(
          model.fName, model.fTitle,
          model.fNbinsX, model.fXLow, model.fXUp,
          model.fNbinsY, model.fYLow, model.fYUp
        );
        prof->SetDirectory(nullptr);
        fProfiles.emplace_back(prof);
        nEvents.emplace_back(0);
        (void)i;
    }
  }
  TProfile2DFromVec(TProfile2DFromVec &&) = default;
  TProfile2DFromVec(const TProfile2DFromVec &) = delete;
  std::shared_ptr<TProfile2D> GetResultPtr() const { return fProfiles[0]; }
  void Initialize() {}
  void InitTask(TTreeReader *, unsigned int) {}
  /// This is a method executed at every entry
  template <typename... ColumnTypes>
  void Exec(unsigned int slot, ColumnTypes const&... values)
  {
    // is this more performant (pass by const ref and use std::forward_as_tuple)
    // or would it be better to pass by value and use std::move?
    // std::tuple<ColumnTypes...> vectors {std::move(values)...};
    auto const& vectors = std::forward_as_tuple(values...);
    // check all vectors have the same length
    if (
      std::get<0>(vectors).size() != std::get<1>(vectors).size()
      || std::get<1>(vectors).size() != std::get<2>(vectors).size()
      || std::get<2>(vectors).size() != std::get<0>(vectors).size()
    ) {
      throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + ": all columns must have the same size per event; column sizes are "
        + std::to_string(std::get<0>(vectors).size()) + ", " + std::to_string(std::get<1>(vectors).size()) + ", " + std::to_string(std::get<2>(vectors).size()));
    }
    auto const nElements = std::get<0>(vectors).size();
    for (unsigned int i = 0; i < nElements; i++) {
      fProfiles[slot]->Fill(std::get<0>(vectors).at(i), std::get<1>(vectors).at(i), std::get<2>(vectors).at(i));
    }
    nEvents[slot]++;
  }
  /// This method is called at the end of the event loop. It is used to merge all the internal THnTs which
  /// were used in each of the data processing slots.
  void Finalize()
  {
    auto &res = fProfiles[0];
    for (auto slot : ROOT::TSeqU(1, fProfiles.size())) {
      res->Add(fProfiles[slot].get());
    }
    // if (normalize) {
    //   unsigned int total = 0;
    //   for (unsigned int nEvent : nEvents) {
    //     total += nEvent;
    //   }
    //   res->Scale(1.0/total);
    // }
  }

  std::string GetActionName(){
    return "TProfile2DFromVec";
  }
};
