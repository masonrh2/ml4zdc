#include "ROOT/RDataFrame.hxx" // for full docs, see https://root.cern/doc/master/classROOT_1_1RDataFrame.html

#include "../include/Axis.hpp"
#include "../include/ZDCModule.hpp"
#include "../include/TProfile2DFromVec.hpp"

#include "RtypesCore.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "TLine.h"
#include "TROOT.h"

#include <limits>

/**
 * TODO: define a TProfile2D method for RDataFrame that accepts vector columns
 */

bool constexpr USE_RPD_COORDINATES = false;
std::string const COORDINATE_TAG = USE_RPD_COORDINATES ? "RPD" : "ATLAS";

std::string const OUT_FILE_PATH = "../plots/position_sensitivity.root";

struct SimulationConfig {
  std::string tag;
  std::string path;
};

std::vector<SimulationConfig> const SIM_CONFIGS = {
  {"topo_1n", "../data/zdcTopoAnalysis_1N.root"},
  {"topo_5n", "../data/zdcTopoAnalysis_5N.root"},
  {"topo_10n", "../data/zdcTopoAnalysis_10N.root"},
};

std::array<Color_t, N_TRUTH_ENERGY_TYPES> constexpr ENERGY_TYPE_COLORS = {
  kRed - 4,
  kOrange + 7,
  kOrange - 2,
  kGreen + 1,
  kAzure + 10,
};

void drawRPDCenterLine(
  unsigned int const side,
  bool const isX,
  Color_t const lineColor = kRed,
  Style_t const lineStyle = kDashed,
  Width_t const lineWidth = 2
) {
  gPad->Update();
  auto const pos = isX ? alignment::RUN3_RPD_POSITION.at(side).x : alignment::RUN3_RPD_POSITION.at(side).y;
  auto line = TLine(pos, gPad->GetUymin(), pos, gPad->GetUymax());
  line.SetLineColor(lineColor);
  line.SetLineStyle(lineStyle);
  line.SetLineWidth(lineWidth);
  line.DrawClone();
}

std::unique_ptr<TCanvas> indicateRPDCenter(
  std::unique_ptr<TCanvas> canvas,
  unsigned int const side,
  bool const isX,
  Color_t const lineColor = kRed,
  Style_t const lineStyle = kDashed,
  Width_t const lineWidth = 2
) {
  canvas->cd();
  drawRPDCenterLine(side, isX, lineColor, lineStyle, lineWidth);
  return canvas;
}

std::unique_ptr<TCanvas> canvasProfile1D(ROOT::RDF::RResultPtr<TProfile> &prof, bool const gridX = true) {
  auto canvas = std::make_unique<TCanvas>(prof->GetName());
  if (gridX) {
    canvas->SetGridx();
  }
  prof->Draw();
  return canvas;
}

void writeProfile1D(ROOT::RDF::RResultPtr<TProfile> &prof, bool const gridX = true) {
  canvasProfile1D(prof, gridX)->Write();
}

std::pair<double, double> getMinMaxWithError(ROOT::RDF::RResultPtr<TProfile2D> &prof) {
  auto min = std::numeric_limits<double>::infinity();
  auto max = -std::numeric_limits<double>::infinity();
  for (int binx = 1; binx <= prof->GetNbinsX(); binx++) {
    for (int biny = 1; biny <= prof->GetNbinsY(); biny++) {
      if (prof->GetBinEntries(prof->GetBin(binx, biny)) == 0) continue;
      auto const content = prof->GetBinContent(binx, biny);
      auto const up = content + prof->GetBinErrorUp(binx, biny);
      auto const low = content - prof->GetBinErrorLow(binx, biny);
      if (up > max) {
        max = up;
      }
      if (low < min) {
        min = low;
      }
    }
  }
  return {min, max};
}

/**
 * @param zoomMargin fraction of axis to use a margin on top and bottom; null to use default draw
 */
void writeProfile2D(
  ROOT::RDF::RResultPtr<TProfile2D> &prof,
  bool const grid = true,
  std::optional<float> zoomMargin = 0.02,
  int const setNCountourLevels = 100
) {
  std::string const name = prof->GetName();
  auto canvas = std::make_unique<TCanvas>(name.c_str());
  if (grid) {
    canvas->SetGrid();
  }
  prof->SetContour(setNCountourLevels);
  if (zoomMargin.has_value()) {
    auto [min, max] = getMinMaxWithError(prof);
    auto const margin = (max - min)*zoomMargin.value();
    min -= margin;
    max += margin;
    prof->SetMinimum(min);
    prof->SetMaximum(max);
  }
  prof->Draw("E");
  canvas->Write(Form("%s_E", name.c_str()));
  prof->Draw("COLZ0");
  canvas->Write(Form("%s_COLZ0", name.c_str()));
}

// this std::span is passed by value...so there is some conversion from std::array happening...
// how does this conversion happen? is it expensive, i.e., does it copy each element?
// ...and is a RResultPtr expensive to copy?
std::pair<double, double> getMinMaxWithError(std::span<ROOT::RDF::RResultPtr<TProfile>> profs) {
  auto maxMax = -std::numeric_limits<double>::infinity();
  auto minMin = std::numeric_limits<double>::infinity();
  for (auto &prof : profs) {
    for (int bin = 1; bin <= prof->GetNbinsX(); bin++) {
      if (prof->GetBinEntries(bin) == 0) continue;
      auto const content = prof->GetBinContent(bin);
      auto const up = content + prof->GetBinErrorUp(bin);
      auto const low = content - prof->GetBinErrorLow(bin);
      if (up > maxMax) {
        maxMax = up;
      }
      if (low < minMin) {
        minMin = low;
      }
    }
  }
  return {minMin, maxMax};
};

struct RPDIndicatorConfig {
  unsigned int side;
  bool horizontalAxisIsX; // else it's Y
  bool drawRodRegion; // draw each channel's rod bounds as shaded region
};

void writeRPDChannelProfile1DHelper(
  std::array<ROOT::RDF::RResultPtr<TProfile>, N_RPD_CHANNELS> &profs,
  std::string const& name,
  RPDIndicatorConfig const& config,
  bool const grid,
  bool const sameScale
){
  auto const [min, max] = getMinMaxWithError(profs);
  auto canvas = std::make_unique<TCanvas>(name.c_str());
  canvas->Divide(4, 4);
  for (unsigned int ch = 0; ch < N_RPD_CHANNELS; ch++) {
    canvas->cd(ch + 1);
    if (grid) gPad->SetGrid();
    if (sameScale) {
      auto *clone = dynamic_cast<TProfile*>(profs.at(ch)->Clone());
      clone->SetMaximum(max);
      clone->Draw();
    } else  {
      profs.at(ch)->Draw();
    }
    drawRPDCenterLine(config.side, config.horizontalAxisIsX);
    if (config.drawRodRegion) {
      auto horizontalBounds = config.horizontalAxisIsX ?
        geometry::getColRodXBounds(geometry::getCol(ch), config.side) : geometry::getRowRodYBounds(geometry::getNoNonsenseRow(ch), true);
      if (!USE_RPD_COORDINATES) {
        auto const& offset = config.horizontalAxisIsX ? alignment::RUN3_RPD_POSITION.at(config.side).x : alignment::RUN3_RPD_POSITION.at(config.side).y;
        for (auto &pos : horizontalBounds) {
          pos += offset;
        }
      }
      gPad->Update();
      auto box = TBox(horizontalBounds.at(0), gPad->GetUymin(), horizontalBounds.at(1), gPad->GetUymax());
      box.SetLineWidth(0);
      box.SetFillStyle(3945);
      box.SetFillColor(kRed);
      box.DrawClone();
    }
  }
  canvas->Write();
}

void writeRPDChannelProfile1D(
  std::array<ROOT::RDF::RResultPtr<TProfile>, N_RPD_CHANNELS> &profs,
  std::string const& name,
  RPDIndicatorConfig const& config,
  bool const grid = true
) {
  writeRPDChannelProfile1DHelper(profs, name, config, grid, false);
  writeRPDChannelProfile1DHelper(profs, name + "_sameScale", config, grid, true);
}

double getMaxMeanWithStdDevs(std::span<ROOT::RDF::RResultPtr<TProfile2D>> profs, float const nStdDev) {
  auto maxMax = -std::numeric_limits<double>::infinity();
  for (auto &prof : profs) {
    auto const val = prof->GetMean(3) + prof->GetStdDev(3)*nStdDev;
    if (val > maxMax) {
      maxMax = val;
    }
  }
  return maxMax;
}

void drawRPDChannelRodRegion2D(unsigned int const side, unsigned int const ch) {
  auto [x1, x2] = geometry::getColRodXBounds(geometry::getCol(ch), side);
  auto [y1, y2] = geometry::getRowRodYBounds(geometry::getNoNonsenseRow(ch), true);
  if (!USE_RPD_COORDINATES)  {
    auto const& xOffset = alignment::RUN3_RPD_POSITION.at(side).x;
    x1 += xOffset;
    x2 += xOffset;
    auto const& yOffset = alignment::RUN3_RPD_POSITION.at(side).y;
    y1 += yOffset;
    y2 += yOffset;
  }
  auto box = TBox(x1, y1, x2, y2);
  // box.SetLineWidth(0);
  // box.SetFillStyle(3945);
  box.SetFillColorAlpha(kBlack, 0);
  box.SetLineColor(kBlack);
  box.SetLineStyle(kDashed);
  box.DrawClone();
}

void writeRPDChannelProfile2D(
  std::array<ROOT::RDF::RResultPtr<TProfile2D>, N_RPD_CHANNELS> &profs,
  std::string const& name,
  unsigned int const side,
  bool const grid = true,
  int const setNCountourLevels = 100
) {
  auto const maxMax = getMaxMeanWithStdDevs(profs, 1);
  auto canvas = std::make_unique<TCanvas>(name.c_str());
  canvas->Divide(4, 4);
  for (unsigned int ch = 0; ch < N_RPD_CHANNELS; ch++) {
    canvas->cd(ch + 1);
    if (grid) gPad->SetGrid();
    // profs.at(ch)->Draw("COLZ0");
    auto *clone = dynamic_cast<TProfile2D*>(profs.at(ch)->Clone());
    clone->SetMaximum(maxMax);
    clone->SetContour(setNCountourLevels);
    clone->Draw("COLZ");
    drawRPDChannelRodRegion2D(side, ch);
  }
  canvas->Write();
}

std::unique_ptr<TCanvas> canvasStack(
  std::array<ROOT::RDF::RResultPtr<TProfile>, N_TRUTH_ENERGY_TYPES> &hists,
  std::string const& name,
  std::string const& xTitle,
  std::string const& yTitle,
  bool const gridX = true,
  bool const doLegend = true
) {
  auto canvas = std::make_unique<TCanvas>(name.c_str());
  canvas->SetRightMargin(0.2);
  if (gridX) {
    canvas->SetGridx();
  }
  static float constexpr legendHalfHeight = 0.2;
  TLegend *legend = new TLegend(
    1 - canvas->GetRightMargin(),
    0.5 - legendHalfHeight,
    1.0,
    0.5 + legendHalfHeight
  );

  auto [minMin, maxMax] = getMinMaxWithError(hists);
  // adjust max, min such that there is this fraction of axis above max and below min
  static double constexpr fracAxisMarginOneSide = 0.02;
  auto const axisMargin = (maxMax - minMin)*fracAxisMarginOneSide;
  maxMax += axisMargin;
  minMin -= axisMargin;

  for (unsigned int i = 0; i < N_TRUTH_ENERGY_TYPES; i++) {
    hists.at(i)->SetLineColor(ENERGY_TYPE_COLORS.at(i));
    legend->AddEntry(hists.at(i).GetPtr(), TRUTH_ENERGY_TYPE_NAMES.at(i).c_str(), "l");
    if (i == 0) {
      auto *firstHistCopy = dynamic_cast<TH1D*>(hists.at(0)->Clone(Form("%s_tmp", hists.at(0)->GetName())));
      firstHistCopy->SetMinimum(minMin);
      firstHistCopy->SetMaximum(maxMax);
      firstHistCopy->SetStats(false);
      firstHistCopy->Draw("E0");
      firstHistCopy->GetXaxis()->SetTitle(xTitle.c_str());
      firstHistCopy->GetYaxis()->SetTitle(yTitle.c_str());
    } else {
      hists.at(i)->Draw("E0 SAME");
    }
  }
  if (doLegend) legend->Draw();
  return canvas;
}

void writeStack(
  std::array<ROOT::RDF::RResultPtr<TProfile>, N_TRUTH_ENERGY_TYPES> &hists,
  std::string const& name,
  std::string const& xTitle,
  std::string const& yTitle,
  bool const gridX = true,
  bool const doLegend = true
) {
  canvasStack(hists, name, xTitle, yTitle, gridX, doLegend)->Write();
}

void writeSim(SimulationConfig const& config) {
  auto const xAxis = USE_RPD_COORDINATES ? axis::xAtRPD.withBins(200) : axis::xAtRPDATLAS.withBins(200);
  auto const yAxis = USE_RPD_COORDINATES ? axis::yAtRPD.withBins(200) : axis::yAtRPDATLAS.withBins(200);
  
  auto df = ROOT::RDataFrame("zdcTree", config.path).Define(
    "truthParticles", RDFHelp::truth::getParticles, RDFHelp::truth::getParticlesInput
  ).Define(
    "truthCentroids", RDFHelp::truth::getTruthCentroids, {"truthParticles"}
  );
  std::array<ROOT::RDF::RResultPtr<TProfile2D>, 2> truthParticleEnergy;

  std::array<ROOT::RDF::RResultPtr<TProfile2D>, 2> ZDCTruthTotalVsPos;
  std::array<std::array<ROOT::RDF::RResultPtr<TProfile>, N_TRUTH_ENERGY_TYPES>, 2> ZDCTruthTypesVsX;
  std::array<std::array<ROOT::RDF::RResultPtr<TProfile>, N_TRUTH_ENERGY_TYPES>, 2> ZDCTruthTypesVsY;

  std::array<std::array<ROOT::RDF::RResultPtr<TProfile2D>, N_SIM_MODULES_USED>, 2> moduleTruthTotalVsPos;
  std::array<std::array<std::array<ROOT::RDF::RResultPtr<TProfile>, N_TRUTH_ENERGY_TYPES>, N_SIM_MODULES_USED>, 2> moduleTruthTypesVsX;
  std::array<std::array<std::array<ROOT::RDF::RResultPtr<TProfile>, N_TRUTH_ENERGY_TYPES>, N_SIM_MODULES_USED>, 2> moduleTruthTypesVsY;

  std::array<std::array<ROOT::RDF::RResultPtr<TProfile2D>, N_SIM_MODULES_USED>, 2> moduleNPhotonsVsPos;
  std::array<std::array<ROOT::RDF::RResultPtr<TProfile>, N_SIM_MODULES_USED>, 2> moduleNPhotonsVsX;
  std::array<std::array<ROOT::RDF::RResultPtr<TProfile>, N_SIM_MODULES_USED>, 2> moduleNPhotonsVsY;

  std::array<std::array<ROOT::RDF::RResultPtr<TProfile2D>, N_RPD_CHANNELS>, 2> RPDChannelNPhotonsVsPos;
  std::array<std::array<ROOT::RDF::RResultPtr<TProfile>, N_RPD_CHANNELS>, 2> RPDChannelNPhotonsVsX;
  std::array<std::array<ROOT::RDF::RResultPtr<TProfile>, N_RPD_CHANNELS>, 2> RPDChannelNPhotonsVsY;
  for (auto const side : SIDES) {
    truthParticleEnergy.at(side) = df.Define(
      "truthParticleProjectedX", RDFHelp::truth::getAllSideParticleProjectedX(side, USE_RPD_COORDINATES), {"truthParticles"}
    ).Define(
      "truthParticleProjectedY", RDFHelp::truth::getAllSideParticleProjectedY(side, USE_RPD_COORDINATES), {"truthParticles"}
    ).Define(
      "truthParticleEnergies", RDFHelp::truth::getAllSideParticleEnergies(side), {"truthParticles"}
    ).Book<ROOT::RVec<float>, ROOT::RVec<float>, ROOT::RVec<float>>(
      TProfile2DFromVec {{
        Form("side%c_posVsTruthParticleEnergy", getSideLabel(side)),
        Form(";Projected X_{%s} @ RPD [mm];Projected Y_{%s} @ RPD [mm];Truth Particle Energy [MeV]", COORDINATE_TAG.c_str(), COORDINATE_TAG.c_str()),
        BINS(xAxis), BINS(yAxis)
      }},
      {"truthParticleProjectedX", "truthParticleProjectedY", "truthParticleEnergies"}
    );
    auto dfWithTruthCentroid = df.Define(
      "truthCentroidX", RDFHelp::truth::getSideTruthXCentroid(side, USE_RPD_COORDINATES), {"truthCentroids"}
    ).Define(
      "truthCentroidY", RDFHelp::truth::getSideTruthYCentroid(side, USE_RPD_COORDINATES), {"truthCentroids"}
    );
    for (unsigned int mod = 0; mod < N_SIM_MODULES_USED; mod++) {
      auto dfWithModuleTruthEnergy = dfWithTruthCentroid.Define(
        "moduleTruthEnergy",
        RDFHelp::getZDCModule<float>(side, static_cast<SimModule>(mod)),
        {MODULE_TRUTH_TOTAL_ENERGIES_BRANCH}
      ).Define(
        "moduleNPhotons",
        RDFHelp::getZDCModule<unsigned int>(side, static_cast<SimModule>(mod)),
        {MODULE_TRUTH_N_PHOTONS_BRANCH}
      );
      moduleTruthTotalVsPos.at(side).at(mod) = dfWithModuleTruthEnergy.Profile2D<float, float, float>(
        {
          Form("side%c_posVs%sModuleTruthTotal", getSideLabel(side), SIM_MODULE_NAMES.at(mod).c_str()),
          Form(";Projected X_{%s} @ RPD [mm];Projected Y_{%s} @ RPD [mm];%s Module Truth Total Energy [MeV]", COORDINATE_TAG.c_str(), COORDINATE_TAG.c_str(), SIM_MODULE_NAMES.at(mod).c_str()),
          BINS(xAxis), BINS(yAxis)
        }, "truthCentroidX", "truthCentroidY", "moduleTruthEnergy"
      );
      moduleNPhotonsVsPos.at(side).at(mod) = dfWithModuleTruthEnergy.Profile2D<float, float, unsigned int>(
        {
          Form("side%c_posVs%sModuleNPhotons", getSideLabel(side), SIM_MODULE_NAMES.at(mod).c_str()),
          Form(";Projected X_{%s} @ RPD [mm];Projected Y_{%s} @ RPD [mm];%s Module N Photons", COORDINATE_TAG.c_str(), COORDINATE_TAG.c_str(), SIM_MODULE_NAMES.at(mod).c_str()),
          BINS(xAxis), BINS(yAxis)
        }, "truthCentroidX", "truthCentroidY", "moduleNPhotons"
      );
      moduleNPhotonsVsX.at(side).at(mod) = dfWithModuleTruthEnergy.Profile1D<float, unsigned int>(
        {
          Form("side%c_xVs%sModuleNPhotons", getSideLabel(side), SIM_MODULE_NAMES.at(mod).c_str()),
          Form(";Projected X_{%s} @ RPD [mm];%s Module N Photons", COORDINATE_TAG.c_str(), SIM_MODULE_NAMES.at(mod).c_str()),
          BINS(xAxis)
        }, "truthCentroidX", "moduleNPhotons"
      );
      moduleNPhotonsVsY.at(side).at(mod) = dfWithModuleTruthEnergy.Profile1D<float, unsigned int>(
        {
          Form("side%c_yVs%sModuleNPhotons", getSideLabel(side), SIM_MODULE_NAMES.at(mod).c_str()),
          Form(";Projected Y_{%s} @ RPD [mm];%s Module N Photons", COORDINATE_TAG.c_str(), SIM_MODULE_NAMES.at(mod).c_str()),
          BINS(yAxis)
        }, "truthCentroidY", "moduleNPhotons"
      );
      for (unsigned int type = 0; type < N_TRUTH_ENERGY_TYPES; type++) {
        std::string const& typeName = TRUTH_ENERGY_TYPE_NAMES.at(type);
        auto dfWithModuleTruthEnergy = dfWithTruthCentroid.Define(
          "moduleTruthEnergy",
          RDFHelp::getZDCModule<float>(side, static_cast<SimModule>(mod)),
          {MODULE_TRUTH_ENERGIES_BRANCHES.at(type)}
        );
        moduleTruthTypesVsX.at(side).at(mod).at(type) = dfWithModuleTruthEnergy.Profile1D<float, float>(
          {
            Form("side%c_xVs%sModuleTruth%s", getSideLabel(side), SIM_MODULE_NAMES.at(mod).c_str(), typeName.c_str()),
            Form(";Projected X_{%s} @ RPD [mm];%s Module Truth %s Energy [MeV]", COORDINATE_TAG.c_str(), SIM_MODULE_NAMES.at(mod).c_str(), typeName.c_str()),
            BINS(xAxis)
          }, "truthCentroidX", "moduleTruthEnergy"
        );
        moduleTruthTypesVsY.at(side).at(mod).at(type) = dfWithModuleTruthEnergy.Profile1D<float, float>(
          {
            Form("side%c_yVs%sModuleTruth%s", getSideLabel(side), SIM_MODULE_NAMES.at(mod).c_str(), typeName.c_str()),
            Form(";Projected Y_{%s} @ RPD [mm];%s Module Truth %s Energy [MeV]", COORDINATE_TAG.c_str(), SIM_MODULE_NAMES.at(mod).c_str(), typeName.c_str()),
            BINS(yAxis)
          }, "truthCentroidY", "moduleTruthEnergy"
        );
      }
    }
    for (unsigned int ch = 0; ch < N_RPD_CHANNELS; ch++) {
      auto dfWithChannelNPhotons = dfWithTruthCentroid.Define(
        "channelNPhotons",
        RDFHelp::getRPDChannel<unsigned int>(side, ch),
        {RPD_CHANNEL_TRUTH_N_PHOTONS_BRANCH}
      );
      RPDChannelNPhotonsVsPos.at(side).at(ch) = dfWithChannelNPhotons.Profile2D<float, float, unsigned int>(
        {
          Form("side%c_posVsRPDChannel%uNPhotons", getSideLabel(side), ch),
          Form(";Projected X_{%s} @ RPD [mm];Projected Y_{%s} @ RPD [mm];RPD Channel %u N Photons", COORDINATE_TAG.c_str(), COORDINATE_TAG.c_str(), ch),
          BINS(xAxis.withBins(100)), BINS(yAxis.withBins(100))
        }, "truthCentroidX", "truthCentroidY", "channelNPhotons"
      );
      RPDChannelNPhotonsVsX.at(side).at(ch) = dfWithChannelNPhotons.Profile1D<float, unsigned int>(
        {
          Form("side%c_xVsRPDChannel%uNPhotons", getSideLabel(side), ch),
          Form(";Projected X_{%s} @ RPD [mm];RPD Channel %u N Photons", COORDINATE_TAG.c_str(), ch),
          BINS(xAxis)
        }, "truthCentroidX", "channelNPhotons"
      );
      RPDChannelNPhotonsVsY.at(side).at(ch) = dfWithChannelNPhotons.Profile1D<float, unsigned int>(
        {
          Form("side%c_yVsRPDChannel%uNPhotons", getSideLabel(side), ch),
          Form(";Projected Y_{%s} @ RPD [mm];RPD Channel %u N Photons", COORDINATE_TAG.c_str(), ch),
          BINS(yAxis)
        }, "truthCentroidY", "channelNPhotons"
      );
    }
    auto dfWithTruthEnergy = dfWithTruthCentroid.Define(
      "truthEnergy", RDFHelp::getSide<float>(side), {TRUTH_TOTAL_ENERGY_BRANCH}
    );
    ZDCTruthTotalVsPos.at(side) = dfWithTruthEnergy.Profile2D<float, float, float>(
      {
        Form("side%c_posVsTruthTotal", getSideLabel(side)),
        Form(";Projected X_{%s} @ RPD [mm];Projected Y_{%s} @ RPD [mm];ZDC Truth Total Energy [MeV]", COORDINATE_TAG.c_str(), COORDINATE_TAG.c_str()),
        BINS(xAxis), BINS(yAxis)
      }, "truthCentroidX", "truthCentroidY", "truthEnergy"
    );
    for (unsigned int type = 0; type < N_TRUTH_ENERGY_TYPES; type++) {
      std::string const& typeName = TRUTH_ENERGY_TYPE_NAMES.at(type);
      auto dfWithTruthEnergy = dfWithTruthCentroid.Define(
        "truthEnergy", RDFHelp::getSide<float>(side), {TRUTH_ENERGY_BRANCHES.at(type)}
      );
      ZDCTruthTypesVsX.at(side).at(type) = dfWithTruthEnergy.Profile1D<float, float>(
        {
          Form("side%c_xVsTruth%s", getSideLabel(side), typeName.c_str()),
          Form(";Projected X_{%s} @ RPD [mm];ZDC Truth %s Energy [MeV]", COORDINATE_TAG.c_str(), typeName.c_str()),
          BINS(xAxis)
        }, "truthCentroidX", "truthEnergy"
      );
      ZDCTruthTypesVsY.at(side).at(type) = dfWithTruthEnergy.Profile1D<float, float>(
        {
          Form("side%c_yVsTruth%s", getSideLabel(side), typeName.c_str()),
          Form(";Projected Y_{%s} @ RPD [mm];ZDC Truth %s Energy [MeV]", COORDINATE_TAG.c_str(), typeName.c_str()),
          BINS(yAxis)
        }, "truthCentroidY", "truthEnergy"
      );
    }
  }

  for (auto const side : SIDES) {
    auto *curDir = static_cast<TDirectory*> gDirectory;
    auto *sideDir = curDir->mkdir(Form("side%c", getSideLabel(side)));
    
    auto *xyDir = sideDir->mkdir("XY");
    xyDir->cd();
    writeProfile2D(truthParticleEnergy.at(side));
    writeProfile2D(ZDCTruthTotalVsPos.at(side));
    for (unsigned int mod = 0; mod < N_SIM_MODULES_USED; mod++) {
      writeProfile2D(moduleTruthTotalVsPos.at(side).at(mod));
    }
    xyDir->mkdir("photons")->cd();
    for (unsigned int mod = 0; mod < N_SIM_MODULES_USED; mod++) {
      writeProfile2D(moduleNPhotonsVsPos.at(side).at(mod));
    }
    writeRPDChannelProfile2D(RPDChannelNPhotonsVsPos.at(side), Form("side%c_grid_posVsRPDChannelNPhotons", getSideLabel(side)), side);

    auto *xDir = sideDir->mkdir("X");
    xDir->cd();
    for (unsigned int type = 0; type < N_TRUTH_ENERGY_TYPES; type++) {
      xDir->mkdir(TRUTH_ENERGY_TYPE_NAMES.at(type).c_str())->cd();
      writeProfile1D(ZDCTruthTypesVsX.at(side).at(type));
      for (unsigned int mod = 0; mod < N_SIM_MODULES_USED; mod++) {
        if (mod == static_cast<unsigned int>(SimModule::RPD) && !USE_RPD_COORDINATES) {
          indicateRPDCenter(canvasProfile1D(moduleTruthTypesVsX.at(side).at(mod).at(type)), side, true)->Write();
          continue;
        }
        writeProfile1D(moduleTruthTypesVsX.at(side).at(mod).at(type));
      }
    }
    xDir->cd();
    writeStack(
      ZDCTruthTypesVsX.at(side),
      Form("side%c_stack_xVsTruthTypes", getSideLabel(side)),
      Form("Projected X_{%s} @ RPD [mm]", COORDINATE_TAG.c_str()),
      "ZDC Truth Energy [MeV]"
    );
    for (unsigned int mod = 0; mod < N_SIM_MODULES_USED; mod++) {
      if (mod == static_cast<unsigned int>(SimModule::RPD) && !USE_RPD_COORDINATES) {
        indicateRPDCenter(canvasStack(
          moduleTruthTypesVsX.at(side).at(mod),
          Form("side%c_stack_xVs%sModuleTruthTypes", getSideLabel(side), SIM_MODULE_NAMES.at(mod).c_str()),
          Form("Projected X_{%s} @ RPD [mm]", COORDINATE_TAG.c_str()),
          Form("%s Module Truth Energy [MeV]", SIM_MODULE_NAMES.at(mod).c_str())
        ), side, true)->Write();
        continue;
      }
      writeStack(
        moduleTruthTypesVsX.at(side).at(mod),
        Form("side%c_stack_xVs%sModuleTruthTypes", getSideLabel(side), SIM_MODULE_NAMES.at(mod).c_str()),
        Form("Projected X_{%s} @ RPD [mm]", COORDINATE_TAG.c_str()),
        Form("%s Module Truth Energy [MeV]", SIM_MODULE_NAMES.at(mod).c_str())
      );
    }
    xDir->mkdir("photons")->cd();
    for (unsigned int mod = 0; mod < N_SIM_MODULES_USED; mod++) {
      if (mod == static_cast<unsigned int>(SimModule::RPD) && !USE_RPD_COORDINATES) {
        indicateRPDCenter(canvasProfile1D(moduleNPhotonsVsX.at(side).at(mod)), side, true)->Write();
        continue;
      }
      writeProfile1D(moduleNPhotonsVsX.at(side).at(mod));
    }
    writeRPDChannelProfile1D(
      RPDChannelNPhotonsVsX.at(side),
      Form("side%c_grid_xVsRPDChannelNPhotons", getSideLabel(side)),
      {side, true, true}
    );

    auto *yDir = sideDir->mkdir("Y");
    yDir->cd();
    for (unsigned int type = 0; type < N_TRUTH_ENERGY_TYPES; type++) {
      yDir->mkdir(TRUTH_ENERGY_TYPE_NAMES.at(type).c_str())->cd();
      writeProfile1D(ZDCTruthTypesVsY.at(side).at(type));
      for (unsigned int mod = 0; mod < N_SIM_MODULES_USED; mod++) {
        if (mod == static_cast<unsigned int>(SimModule::RPD) && !USE_RPD_COORDINATES) {
          indicateRPDCenter(canvasProfile1D(moduleTruthTypesVsY.at(side).at(mod).at(type)), side, false)->Write();
          continue;
        }
        writeProfile1D(moduleTruthTypesVsY.at(side).at(mod).at(type));
      }
    }
    yDir->cd();
    writeStack(
      ZDCTruthTypesVsY.at(side),
      Form("side%c_stack_yVsTruthTypes", getSideLabel(side)),
      Form("Projected Y_{%s} @ RPD [mm]", COORDINATE_TAG.c_str()),
      "ZDC Truth Energy [MeV]"
    );
    for (unsigned int mod = 0; mod < N_SIM_MODULES_USED; mod++) {
      if (mod == static_cast<unsigned int>(SimModule::RPD) && !USE_RPD_COORDINATES) {
        indicateRPDCenter(canvasStack(
          moduleTruthTypesVsY.at(side).at(mod),
          Form("side%c_stack_yVs%sModuleTruthTypes", getSideLabel(side), SIM_MODULE_NAMES.at(mod).c_str()),
          Form("Projected Y_{%s} @ RPD [mm]", COORDINATE_TAG.c_str()),
          Form("%s Module Truth Energy [MeV]", SIM_MODULE_NAMES.at(mod).c_str())
        ), side, false)->Write();
        continue;
      }
      writeStack(
        moduleTruthTypesVsY.at(side).at(mod),
        Form("side%c_stack_yVs%sModuleTruthTypes", getSideLabel(side), SIM_MODULE_NAMES.at(mod).c_str()),
        Form("Projected Y_{%s} @ RPD [mm]", COORDINATE_TAG.c_str()),
        Form("%s Module Truth Energy [MeV]", SIM_MODULE_NAMES.at(mod).c_str())
      );
    }
    yDir->mkdir("photons")->cd();
    for (unsigned int mod = 0; mod < N_SIM_MODULES_USED; mod++) {
      if (mod == static_cast<unsigned int>(SimModule::RPD) && !USE_RPD_COORDINATES) {
        indicateRPDCenter(canvasProfile1D(moduleNPhotonsVsY.at(side).at(mod)), side, false)->Write();
        continue;
      }
      writeProfile1D(moduleNPhotonsVsY.at(side).at(mod));
    }
    writeRPDChannelProfile1D(
      RPDChannelNPhotonsVsY.at(side),
      Form("side%c_grid_yVsRPDChannelNPhotons", getSideLabel(side)),
      {side, false, true}
    );

    curDir->cd();
  }
}

void plot_position_sensitivity() {
  ROOT::EnableImplicitMT();
  TFile *outFile = TFile::Open(OUT_FILE_PATH.c_str(), "RECREATE");
  for (auto const& config : SIM_CONFIGS) {
    outFile->mkdir(config.tag.c_str())->cd();
    writeSim(config);
  }
  outFile->Close();
}
