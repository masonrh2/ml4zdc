#include "ROOT/RDF/RInterface.hxx"
#include "ROOT/RDataFrame.hxx"

#include "../include/ZDCModule.hpp"
#include "../include/Axis.hpp"

#include "TLegend.h"
#include "TCanvas.h"
#include "THStack.h"

struct SimulationConfig {
  std::string tag;
  std::string path;
  float maxZDCModuleTruthEnergy; // MeV
  float maxZDCModuleRecoAmp;
  float maxRPDTruthEnergy; // MeV
  float maxRPDRecoSumAmp;
};

std::array<Color_t, N_SIM_MODULES_USED> constexpr SIM_MODULE_COLORS {
  kRed - 4,
  kOrange + 7,
  kOrange - 2,
  kGreen + 1,
  kAzure + 10,
  kAzure - 3,
};

std::array<Color_t, N_RECO_MODULES> constexpr RECO_MODULE_COLORS {
  kRed - 4,
  kOrange - 2,
  kGreen + 1,
  kAzure - 3,
};

std::vector<SimulationConfig> const SIM_CONFIGS = {
  {
    "topo_1n", "../data/zdcTopoAnalysis_1N.root",
    4e6, 2e3, 80e3, 3e4,
  },
  {
    "topo_5n", "../data/zdcTopoAnalysis_5N.root",
    8e6, 5e3, 160e3, 6e4
  },
  {
    "topo_10n", "../data/zdcTopoAnalysis_10N.root",
    8e6*2, 5e3*2, 160e3*2, 8e4
  },
};

std::string const OUT_FILE_PATH = "../plots/module_energy.root";

void write2D(ROOT::RDF::RResultPtr<TH2D> &hist, bool const logz = false, bool const profileX = true) {
  auto canvas = std::make_unique<TCanvas>(hist->GetName());
  if (logz) {
    gPad->SetLogz();
  }
  hist->Draw("COLZ");
  if (profileX) {
    auto *prof = hist->ProfileX();
    prof->SetMarkerStyle(kFullCircle);
    prof->SetMarkerColor(kBlack);
    prof->Draw("SAME");
  }
  canvas->Write();
}

void writePlot(SimulationConfig const& config) {
  axis::Axis const recoZDCModuleAxis {128, 0, config.maxZDCModuleRecoAmp};
  axis::Axis const truthZDCModuleAxis {128, 0, config.maxZDCModuleTruthEnergy};
  axis::Axis const recoRPDAxis {128, 0, config.maxRPDRecoSumAmp};
  axis::Axis const truthRPDAxis {128, 0, config.maxRPDTruthEnergy};
  auto dataframe = ROOT::RDataFrame("zdcTree", config.path);
  std::array<std::array<ROOT::RDF::RResultPtr<TH1D>, N_SIM_MODULES_USED>, 2> hTruthEnergy {};
  // std::array<ROOT::RDF::RResultPtr<TH1D>, 2> hZDCRecoEnergy {};
  std::array<std::array<ROOT::RDF::RResultPtr<TH1D>, N_RECO_MODULES>, 2> hZDCModuleRecoEnergy {};
  std::array<std::array<ROOT::RDF::RResultPtr<TH2D>, N_RECO_MODULES>, 2> hModuleCorrelation {};
  std::array<ROOT::RDF::RResultPtr<TH2D>, 2> hRPDCorrelation {};
  for (auto const& side : SIDES) {
    ROOT::RDF::RInterface<ROOT::Detail::RDF::RLoopManager, void> dataframeUnpacked = dataframe;
    for (unsigned int mod = 0; mod < N_SIM_MODULES_USED; mod++) {
      dataframeUnpacked = dataframeUnpacked.Define(
        SIM_MODULE_NAMES.at(mod) + "_truth",
        RDFHelp::getZDCModule<float>(side, static_cast<SimModule>(mod)),
        {MODULE_TRUTH_TOTAL_ENERGIES_BRANCH}
      );
    }
    for (unsigned int mod = 0; mod < N_RECO_MODULES; mod++) {
      auto const recoMod = static_cast<RecoModule>(mod);
      auto const *moduleName = RECO_MODULE_NAMES.at(mod).c_str();
      auto dfValidMod = dataframeUnpacked.Filter(
        RDFHelp::reco::ZDCModule::checkValid(side, recoMod), {"zdc_ZdcModuleStatus"}
      ).Define(
        RECO_MODULE_NAMES.at(mod) + "_reco", RDFHelp::getZDCModule<float>(side, recoMod), {"zdc_ZdcModuleAmp"}
      );
      hZDCModuleRecoEnergy.at(side).at(mod) = dfValidMod.Histo1D<float>(
        {
          Form("side%c_%s_reco", getSideLabel(side), moduleName),
          Form(";%s Module Reco. Amplitude [ADC]; Count", moduleName),
          BINS(recoZDCModuleAxis)
        }, RECO_MODULE_NAMES.at(mod) + "_reco"
      );
      hModuleCorrelation.at(side).at(mod) = dfValidMod.Histo2D<float, float>(
        {
          Form("side%c_%s_correlation", getSideLabel(side), moduleName),
          Form(";%s Module Truth Total Energy [MeV];%s Reco. Amplitude [ADC]; Count", moduleName, moduleName),
          BINS(truthZDCModuleAxis), BINS(recoZDCModuleAxis)
        }, SIM_MODULE_NAMES.at(mod) + "_truth", RECO_MODULE_NAMES.at(mod) + "_reco"
      );
    }
    {
      // RPD
      hRPDCorrelation.at(side) = dataframeUnpacked.Filter(
        RDFHelp::reco::RPD::checkValid(side), {"zdc_RpdSideStatus"}
      ).Define(
        "RPDSumAmp", RDFHelp::reco::RPD::getSumSumADC(side), {"zdc_RpdChannelAmplitude"}
      ).Histo2D<float, float>(
        {
          Form("side%c_RPD_correlation", getSideLabel(side)),
          ";RPD Truth Total Energy [MeV];RPD Sum Reco. Amplitude [ADC];Count",
          BINS(truthRPDAxis), BINS(recoRPDAxis)
        }, "RPD_truth", "RPDSumAmp"
      );
    }
    for (unsigned int mod = 0; mod < N_SIM_MODULES_USED; mod++) {
      hTruthEnergy.at(side).at(mod) = dataframeUnpacked.Histo1D<float>(
        {
          Form("side%c_%s_truth", getSideLabel(side), SIM_MODULE_NAMES.at(mod).c_str()),
          Form(";%s Module Truth Total Energy [MeV];Count", SIM_MODULE_NAMES.at(mod).c_str()),
          BINS(truthZDCModuleAxis)
        },
        SIM_MODULE_NAMES.at(mod) + "_truth"
      );
    }
  }

  for (auto const side : SIDES) {
    {
      auto canvas = std::make_unique<TCanvas>(Form("side%c_moduleTruthEnergies", getSideLabel(side)));
      THStack *stack = new THStack();
      stack->SetTitle(config.tag.c_str());
      TLegend *legend = new TLegend(
        0.7,
        0.55,
        1 - gPad->GetRightMargin(),
        1 - gPad->GetTopMargin()
      );
      for (unsigned int i = 0; i < N_SIM_MODULES_USED; i++) {
        auto const mod = MODULE_PLOT_ORDER.at(i);
        auto & hist = hTruthEnergy.at(side).at(mod);
        auto const& name = SIM_MODULE_NAMES.at(mod);
        hist->SetLineColor(SIM_MODULE_COLORS.at(i));
        stack->Add(hist.GetPtr(), "HIST");
        legend->AddEntry(hist.GetPtr(), name.c_str(), "l");
      }
      stack->Draw("NOSTACK");
      stack->GetXaxis()->SetTitle("Module Truth Total Energy [MeV]");
      legend->Draw();
      canvas->Write();

      canvas->SetLogy();
      canvas->Write(Form("%s_logy", canvas->GetName()));
    }
    {
      auto canvas = std::make_unique<TCanvas>(Form("side%c_moduleRecoEnergies", getSideLabel(side)));
      THStack *stack = new THStack();
      stack->SetTitle(config.tag.c_str());
      TLegend *legend = new TLegend(
        0.7,
        0.55,
        1 - gPad->GetRightMargin(),
        1 - gPad->GetTopMargin()
      );
      for (unsigned int mod = 0; mod < N_RECO_MODULES; mod++) {
        auto & hist = hZDCModuleRecoEnergy.at(side).at(mod);
        auto const& name = RECO_MODULE_NAMES.at(mod);
        hist->SetLineColor(RECO_MODULE_COLORS.at(mod));
        stack->Add(hist.GetPtr(), "HIST");
        legend->AddEntry(hist.GetPtr(), name.c_str(), "l");
      }
      stack->Draw("NOSTACK");
      stack->GetXaxis()->SetTitle("Module Reco. Amplitude [ADC]");
      legend->Draw();
      canvas->Write();

      canvas->SetLogy();
      canvas->Write(Form("%s_logy", canvas->GetName()));
    }
    for (unsigned int mod = 0; mod < N_RECO_MODULES; mod++) {
      write2D(hModuleCorrelation.at(side).at(mod), true);
    }
    write2D(hRPDCorrelation.at(side), true);
  }
}

void plot_module_energy() {
  ROOT::EnableImplicitMT();
  TFile *outFile = TFile::Open(OUT_FILE_PATH.c_str(), "RECREATE");
  for (auto const& config : SIM_CONFIGS) {
    outFile->mkdir(config.tag.c_str())->cd();
    writePlot(config);
  }
  outFile->Close();
}
