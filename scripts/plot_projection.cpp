#include "ROOT/RDataFrame.hxx" // for full docs, see https://root.cern/doc/master/classROOT_1_1RDataFrame.html

#include "../include/Axis.hpp"
#include "../include/ZDCModule.hpp"

#include "TCanvas.h"

struct SimulationConfig {
  std::string tag;
  std::string path;
  std::pair<int, int> nTruthParticlesRange;
  axis::Axis getNTruthParticlesAxis() const {
    return {
      nTruthParticlesRange.second - nTruthParticlesRange.first + 1,
      static_cast<float>(nTruthParticlesRange.first),
      static_cast<float>(nTruthParticlesRange.second + 1),
    };
  }
};

std::vector<SimulationConfig> const SIM_CONFIGS = {
  {"topo_1n", "../data/zdcTopoAnalysis_1N.root", {0, 5}},
  {"topo_5n", "../data/zdcTopoAnalysis_5N.root", {0, 10}},
  {"topo_10n", "../data/zdcTopoAnalysis_10N.root", {0, 20}},
};

std::string const OUT_FILE_PATH = "../plots/projection.root";

void write1D(ROOT::RDF::RResultPtr<TH1D> &hist, bool const logy = true) {
  auto canvas = std::make_unique<TCanvas>(hist->GetName());
  if (logy) {
    gPad->SetLogy();
  }
  hist->Draw("HIST");
  canvas->Write();
}

void write2D(ROOT::RDF::RResultPtr<TH2D> &hist, bool const grid = true, bool const logz = false) {
  auto canvas = std::make_unique<TCanvas>(hist->GetName());
  if (logz) {
    gPad->SetLogz();
  }
  if (grid) {
    gPad->SetGrid();
  }
  hist->Draw("COLZ");
  canvas->Write();
}

void writePlots(SimulationConfig const& config) {
  auto df = ROOT::RDataFrame("zdcTree", config.path).Define(
    "truthParticles", RDFHelp::truth::getParticles, RDFHelp::truth::getParticlesInput
  ).Define(
    "truthCentroids", RDFHelp::truth::getTruthCentroids, {"truthParticles"}
  );
  std::array<ROOT::RDF::RResultPtr<TH1D>, 2> nTruthParticles;
  std::array<ROOT::RDF::RResultPtr<TH2D>, 2> truthCentroidRPD;
  std::array<ROOT::RDF::RResultPtr<TH2D>, 2> truthCentroidATLAS;
  std::array<ROOT::RDF::RResultPtr<TH2D>, 2> recoCentroid;
  std::array<ROOT::RDF::RResultPtr<TH2D>, 2> xTruthVsRecoCentroid;
  std::array<ROOT::RDF::RResultPtr<TH2D>, 2> yTruthVsRecoCentroid;
  for (auto const side : SIDES) {
    auto dfWithTruthCentroid = df.Define(
      "truthCentroidXRPD", RDFHelp::truth::getSideTruthXCentroid(side, true), {"truthCentroids"}
    ).Define(
      "truthCentroidYRPD", RDFHelp::truth::getSideTruthYCentroid(side, true), {"truthCentroids"}
    ).Define(
      "truthCentroidXATLAS", RDFHelp::truth::getSideTruthXCentroid(side, false), {"truthCentroids"}
    ).Define(
      "truthCentroidYATLAS", RDFHelp::truth::getSideTruthYCentroid(side, false), {"truthCentroids"}
    ).Define(
      "nTruthParticles", RDFHelp::truth::getSideNTruthParticles(side), {"truthCentroids"}
    );
    nTruthParticles.at(side) = dfWithTruthCentroid.Histo1D<unsigned int>(
      {Form("side%c_nTruthParticles", getSideLabel(side)), ";# Truth Particles;Count", BINS(config.getNTruthParticlesAxis())}, "nTruthParticles"
    );
    truthCentroidRPD.at(side) = dfWithTruthCentroid.Histo2D<float, float>(
      {Form("side%c_truthCentroidRPD", getSideLabel(side)), ";x_{RPD} [mm];y_{RPD} [mm];Count", BINS(axis::xAtRPD), BINS(axis::yAtRPD)},
      "truthCentroidXRPD", "truthCentroidYRPD"
    );
    truthCentroidATLAS.at(side) = dfWithTruthCentroid.Histo2D<float, float>(
      {Form("side%c_truthCentroidATLAS", getSideLabel(side)), ";x_{ATLAS} [mm];y_{ATLAS} [mm];Count", BINS(axis::xAtRPDATLAS), BINS(axis::yAtRPDATLAS)},
      "truthCentroidXATLAS", "truthCentroidYATLAS"
    );
    {
      // VALID CENTROID
      auto dfCentroidValid = dfWithTruthCentroid.Filter(
        RDFHelp::reco::centroid::checkValid(side), {"zdc_centroidStatus"}, Form("side %c centroid valid", getSideLabel(side))
      ).Define(
        "recoCentroidX", RDFHelp::getSide<float>(side), {"zdc_xCentroid"}
      ).Define(
        "recoCentroidY", RDFHelp::getSide<float>(side), {"zdc_yCentroid"}
      );
      recoCentroid.at(side) = dfCentroidValid.Histo2D<float, float>(
        {Form("side%c_recoCentroid", getSideLabel(side)), ";x [mm];y [mm];Count", BINS(axis::xAtRPD), BINS(axis::yAtRPD)},
        "recoCentroidX", "recoCentroidY"
      );
      xTruthVsRecoCentroid.at(side) = dfCentroidValid.Histo2D<float, float>(
        {Form("side%c_xTruthVsRecoCentroid", getSideLabel(side)), ";Truth Particle Centroid x [mm];Reconstructed Centroid x [mm];Count", BINS(axis::xAtRPD), BINS(axis::xAtRPD)},
        "truthCentroidXRPD", "recoCentroidX"
      );
      yTruthVsRecoCentroid.at(side) = dfCentroidValid.Histo2D<float, float>(
        {Form("side%c_yTruthVsRecoCentroid", getSideLabel(side)), ";Truth Particle Centroid y [mm];Reconstructed Centroid y [mm];Count", BINS(axis::yAtRPD), BINS(axis::yAtRPD)},
        "truthCentroidYRPD", "recoCentroidY"
      );
    }
  }
  for (auto const side : SIDES) {
    write1D(nTruthParticles.at(side));
    write2D(truthCentroidRPD.at(side));
    write2D(truthCentroidATLAS.at(side));
    write2D(recoCentroid.at(side));
    write2D(xTruthVsRecoCentroid.at(side));
    write2D(yTruthVsRecoCentroid.at(side));
  }
}

void plot_projection() {
  ROOT::EnableImplicitMT();
  TFile *outFile = TFile::Open(OUT_FILE_PATH.c_str(), "RECREATE");
  for (auto const& config : SIM_CONFIGS) {
    outFile->mkdir(config.tag.c_str())->cd();
    writePlots(config);
  }
  outFile->Close();
}
