#include "ROOT/RDataFrame.hxx"

#include "../include/ZDCModule.hpp"
#include "../include/Axis.hpp"

#include "TLegend.h"
#include "TCanvas.h"
#include "THStack.h"

std::string const OUT_FILE_PATH = "../plots/module_truth_energy_types.root";

struct ModuleAxes {
 public:
  ModuleAxes(
    float const maxTotal,
    float const maxEM,
    float const maxHAD1,
    float const maxHAD2,
    float const maxHAD3,
    float const maxRPD,
    float const maxBRAN
  ) : m_totalAxis {nBins, -maxTotal*negativeAxisFactor, maxTotal},
      m_moduleAxes{{
        {nBins, -maxEM*negativeAxisFactor, maxEM},
        {nBins, -maxHAD1*negativeAxisFactor, maxHAD1},
        {nBins, -maxHAD2*negativeAxisFactor, maxHAD2},
        {nBins, -maxHAD3*negativeAxisFactor, maxHAD3},
        {nBins, -maxRPD*negativeAxisFactor, maxRPD},
        {nBins, -maxBRAN*negativeAxisFactor, maxBRAN},
      }}
  {}
  axis::Axis const& getTotalAxis() const {
    return m_totalAxis;
  }
  axis::Axis const& getModuleAxis(unsigned int const mod) const {
    return m_moduleAxes.at(mod);
  }
 private:
  static int constexpr nBins = 2048;
  static float constexpr negativeAxisFactor = 0.1;
  axis::Axis m_totalAxis;
  std::array<axis::Axis, N_SIM_MODULES_USED> m_moduleAxes;
};

struct SimulationConfig {
  std::string tag;
  std::string path;
  ModuleAxes axes;
};

/**
 * @brief scaling the single neutron axis by the number of neutrons is a good guess for the
 * axis limits of the multi-neutron simulations, but it's not perfect
 */

std::vector<SimulationConfig> const SIM_CONFIGS = {
  {
    "topo_1n", "../data/zdcTopoAnalysis_1N.root",
    ModuleAxes(
      3e6,
      2e6,
      2e6,
      1.5e6,
      1e6,
      8e4,
      6e5
    )
  },
  {
    "topo_5n", "../data/zdcTopoAnalysis_5N.root",
    ModuleAxes(
      3e6*5,
      2e6*5,
      2e6*5,
      1.5e6*5,
      1e6*5,
      8e4*5,
      6e5*5
    )
  },
  {
    "topo_10n", "../data/zdcTopoAnalysis_10N.root",
    ModuleAxes(
      3e6*10,
      2e6*10,
      2e6*10,
      1.5e6*10,
      1e6*10,
      8e4*10,
      6e5*10
    )
  },
};

namespace EMNonEM = RDFHelp::truth::EMNonEM;
namespace visible = RDFHelp::truth::visible;

auto constexpr EXT_N_TRUTH_ENERGY_TYPES = N_TRUTH_ENERGY_TYPES + 2;
std::array<std::string, EXT_N_TRUTH_ENERGY_TYPES> const EXT_TRUTH_ENERGY_TYPE_NAMES {
  TRUTH_ENERGY_TYPE_NAMES.at(0),
  TRUTH_ENERGY_TYPE_NAMES.at(1),
  TRUTH_ENERGY_TYPE_NAMES.at(2),
  TRUTH_ENERGY_TYPE_NAMES.at(3),
  TRUTH_ENERGY_TYPE_NAMES.at(4),
  "EM + NonEM",
  "Visible",
};

std::array<Color_t, EXT_N_TRUTH_ENERGY_TYPES> constexpr EXT_ENERGY_TYPE_COLORS = {
  kPink,
  kOrange + 1,
  kOrange,
  kGreen - 3,
  kAzure + 8,
  kAzure - 5,
  kViolet - 5,
};

std::array<std::string, EXT_N_TRUTH_ENERGY_TYPES> const EXT_TRUTH_ENERGY_BRANCHES {
  TRUTH_ENERGY_BRANCHES.at(0),
  TRUTH_ENERGY_BRANCHES.at(1),
  TRUTH_ENERGY_BRANCHES.at(2),
  TRUTH_ENERGY_BRANCHES.at(3),
  TRUTH_ENERGY_BRANCHES.at(4),
  EMNonEM::ZDC::outputColumn,
  visible::ZDC::outputColumn,
};

std::array<std::string, EXT_N_TRUTH_ENERGY_TYPES> const EXT_MODULE_TRUTH_ENERGIES_BRANCHES {
  MODULE_TRUTH_ENERGIES_BRANCHES.at(0),
  MODULE_TRUTH_ENERGIES_BRANCHES.at(1),
  MODULE_TRUTH_ENERGIES_BRANCHES.at(2),
  MODULE_TRUTH_ENERGIES_BRANCHES.at(3),
  MODULE_TRUTH_ENERGIES_BRANCHES.at(4),
  EMNonEM::ZDCModule::outputColumn,
  visible::ZDCModule::outputColumn,
};

void writeStack(
  std::string const& stackName,
  std::array<ROOT::RDF::RResultPtr<TH1D>, EXT_N_TRUTH_ENERGY_TYPES> &hists,
  std::string const& xTitle
) {
  auto canvas = std::make_unique<TCanvas>(stackName.c_str());
  canvas->SetLogy();
  auto *stack = new THStack();
  TLegend *legend = new TLegend(
    0.7,
    0.55,
    1 - gPad->GetRightMargin(),
    1 - gPad->GetTopMargin()
  );
  for (unsigned int type = 0; type < EXT_N_TRUTH_ENERGY_TYPES; type++) {
    auto hist = hists.at(type);
    hist->SetLineColor(EXT_ENERGY_TYPE_COLORS.at(type));
    stack->Add(hist.GetPtr(), "HIST");
    legend->AddEntry(hist.GetPtr(), EXT_TRUTH_ENERGY_TYPE_NAMES.at(type).c_str(), "l");
  }
  stack->Draw("NOSTACK");
  stack->GetXaxis()->SetTitle(xTitle.c_str());
  legend->Draw();
  canvas->Write();
}

void writeSim(SimulationConfig const& config) {
  // make (declare) the histograms
  auto dataframe = ROOT::RDataFrame("zdcTree", config.path).Define(
    EMNonEM::ZDC::outputColumn, EMNonEM::ZDC::calculate, EMNonEM::ZDC::inputColumns
  ).Define(
    EMNonEM::ZDCModule::outputColumn, EMNonEM::ZDCModule::calculate, EMNonEM::ZDCModule::inputColumns
  ).Define(
    visible::ZDC::outputColumn, visible::ZDC::calculate, visible::ZDC::inputColumns
  ).Define(
    visible::ZDCModule::outputColumn, visible::ZDCModule::calculate, visible::ZDCModule::inputColumns
  );
  std::array<std::array<ROOT::RDF::RResultPtr<TH1D>, EXT_N_TRUTH_ENERGY_TYPES>, 2> truthEnergyTypes;
  std::array<std::array<std::array<ROOT::RDF::RResultPtr<TH1D>, EXT_N_TRUTH_ENERGY_TYPES>, N_SIM_MODULES_USED>, 2> moduleTruthEnergyTypes;
  for (auto const& side : SIDES) {
    for (unsigned int type = 0; type < EXT_N_TRUTH_ENERGY_TYPES; type++) {
      truthEnergyTypes.at(side).at(type) = dataframe.Define(
        "typeEnergy", RDFHelp::getSide<float>(side), {EXT_TRUTH_ENERGY_BRANCHES.at(type)}
      ).Histo1D<float>(
        {
          Form("ZDCTruth%sEnergy", EXT_TRUTH_ENERGY_TYPE_NAMES.at(type).c_str()),
          "", BINS(config.axes.getTotalAxis())
        }, "typeEnergy"
      );
      for (unsigned int mod = 0; mod < N_SIM_MODULES_USED; mod++) {
        moduleTruthEnergyTypes.at(side).at(mod).at(type) = dataframe.Define(
          "typeModuleEnergy",
          RDFHelp::getZDCModule<float>(side, static_cast<SimModule>(mod)),
          {EXT_MODULE_TRUTH_ENERGIES_BRANCHES.at(type)}
        ).Histo1D<float>(
          {
            Form("%sModuleTruth%sEnergy", SIM_MODULE_NAMES.at(mod).c_str(), EXT_TRUTH_ENERGY_TYPE_NAMES.at(type).c_str()),
            "", BINS(config.axes.getModuleAxis(mod))
          }, "typeModuleEnergy"
        );
      }
    }
  }
  // write the plots
  for (auto const side : SIDES) {
    auto *curDir = static_cast<TDirectory*> gDirectory;
    curDir->mkdir(Form("side%c", getSideLabel(side)))->cd();
    writeStack("ZDCEnergyTypes", truthEnergyTypes.at(side), "ZDC Truth Energy [MeV]");
    for (unsigned int mod = 0; mod < N_SIM_MODULES_USED; mod++) {
      std::string const& moduleName = SIM_MODULE_NAMES.at(mod);
      writeStack(
        Form("%sModuleEnergyTypes", moduleName.c_str()),
        moduleTruthEnergyTypes.at(side).at(mod),
        Form("%s Module Truth Energy [MeV]", moduleName.c_str())
      );
    }
    curDir->cd();
  }
}

void plot_module_truth_energy_types() {
  ROOT::EnableImplicitMT();
  TFile *outFile = TFile::Open(OUT_FILE_PATH.c_str(), "RECREATE");
  for (auto const& config : SIM_CONFIGS) {
    outFile->mkdir(config.tag.c_str())->cd();
    writeSim(config);
  }
  outFile->Close();
}
