#include "ROOT/RDataFrame.hxx" // for full docs, see https://root.cern/doc/master/classROOT_1_1RDataFrame.html

#include "../include/Axis.hpp"
#include "../include/ZDCModule.hpp"

#include "ROOT/RResultPtr.hxx"
#include "TCanvas.h"

/**
 * @brief TODO: estimate how many interaction lengths each module is by calculating the number of
 * incident particles that startyed showering in that module
 * an open question is what the energy threshold (and which type(s) of truth energy) for shower "start"?
 * can number of photons play any role here...?
 *
 * this discussion of "punch-through" is only really relevant for single neutron simulations, since
 * the probability of multiple neutrons not interacting is not the same as that for a single neutron.
 */

std::string const OUT_FILE_PATH = "../plots/punch_through.root";

struct SimulationConfig {
  std::string tag;
  std::string path;
};

std::vector<SimulationConfig> const SIM_CONFIGS = {
  {"topo_1n", "../data/zdcTopoAnalysis_1N.root"},
};

axis::Axis constexpr SIM_MODULES_AXIS {N_SIM_MODULES_USED, 0, N_SIM_MODULES_USED};

/**
 * @brief get a function which returns the plot order indices of modules which the incident particles
 * punched through on the given side
 */
std::function<std::vector<unsigned int>(ROOT::VecOps::RVec<float> const& zdc_ZdcModuleTruthTotal)> getModulesPunchedThrough(
  unsigned int const side, double const thresh
) {
  return [side, thresh] (ROOT::VecOps::RVec<float> const& zdc_ZdcModuleTruthTotal) {
    std::vector<unsigned int> modulesPunchedThrough;
    for (unsigned int modPlotOrderIndex = 0; modPlotOrderIndex < MODULE_PLOT_ORDER.size(); modPlotOrderIndex++) {
      auto const modIndex = MODULE_PLOT_ORDER.at(modPlotOrderIndex);
      auto const mod = static_cast<SimModule>(modIndex);
      auto const moduleTotalEnergy = zdc_ZdcModuleTruthTotal.at(getZDCModuleIndex(side, mod));
      if (moduleTotalEnergy > thresh) {
        // the buck stops here
        return modulesPunchedThrough;
      }
      modulesPunchedThrough.push_back(modPlotOrderIndex);
    }
    return modulesPunchedThrough;
  };
}

void writePunchThroughHist(ROOT::RDF::RResultPtr<TH1D> &hist, ROOT::RDF::RResultPtr<unsigned long long> &nEvents) {
  hist->Sumw2(true);
  for (unsigned int modPlotOrderIndex = 0; modPlotOrderIndex < MODULE_PLOT_ORDER.size(); modPlotOrderIndex++) {
    hist->GetXaxis()->SetBinLabel(modPlotOrderIndex + 1, SIM_MODULE_NAMES.at(MODULE_PLOT_ORDER.at(modPlotOrderIndex)).c_str());
  }

  auto canvas = std::make_unique<TCanvas>(hist->GetName());
  hist->Draw();
  canvas->Write();

  std::string const cloneName = Form("%s_norm", hist->GetName());
  auto canvasNorm = std::make_unique<TCanvas>(cloneName.c_str());
  auto *clone = dynamic_cast<TH1D*>(hist->Clone(cloneName.c_str()));
  clone->Scale(1.0/nEvents.GetValue());
  clone->GetYaxis()->SetTitle("Probability");
  clone->Draw();
  canvasNorm->Write();
}

void writeSim(SimulationConfig const& config) {
  std::array<ROOT::RDF::RResultPtr<unsigned long long>, 2> nEvents;
  std::array<ROOT::RDF::RResultPtr<TH1D>, 2> modulePunchThrough;
  auto df = ROOT::RDataFrame("zdcTree", config.path).Define(
    RDFHelp::truth::visible::ZDCModule::outputColumn,
    RDFHelp::truth::visible::ZDCModule::calculate,
    RDFHelp::truth::visible::ZDCModule::inputColumns
  ).Define(
    RDFHelp::truth::EMNonEM::ZDCModule::outputColumn,
    RDFHelp::truth::EMNonEM::ZDCModule::calculate,
    RDFHelp::truth::EMNonEM::ZDCModule::inputColumns
  );
  for (auto const side : SIDES) {
    for (unsigned int modIndex = 0; modIndex < N_SIM_MODULES_USED; modIndex++) {
      auto const mod = static_cast<SimModule>(modIndex);
      /**
       * note that when the column (or "branch", if you preder) you use to fill a histogram is a vector type,
       * RDataFrame simply Fill()'s the histogram with every entry in the vector :)
       */
      modulePunchThrough.at(side) = df.Define(
        "modulesPunchedThrough", getModulesPunchedThrough(side, 50 /* MeV*/), {RDFHelp::truth::visible::ZDCModule::outputColumn}
      ).Histo1D<std::vector<unsigned int>>(
        {Form("punchThrough"), ";Module;Count", BINS(SIM_MODULES_AXIS)}, "modulesPunchedThrough"
      );
      // itthe count isn't different per side right now, but it could be if we had filters
      nEvents.at(side) = df.Count();
    }
  }
  TDirectory * const curDir = gDirectory;
  for (auto const side : SIDES) {
    curDir->mkdir(Form("side%c", getSideLabel(side)))->cd();
    writePunchThroughHist(modulePunchThrough.at(side), nEvents.at(side));
  }
}

void plot_punch_through() {
  ROOT::EnableImplicitMT();
  TFile *outFile = TFile::Open(OUT_FILE_PATH.c_str(), "RECREATE");
  for (auto const& config : SIM_CONFIGS) {
    outFile->mkdir(config.tag.c_str())->cd();
    writeSim(config);
  }
  outFile->Close();
}
