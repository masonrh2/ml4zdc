#include "ROOT/RDF/InterfaceUtils.hxx"
#include "ROOT/RDF/RInterface.hxx"
#include "ROOT/RDataFrame.hxx"

#include "../include/ZDCModule.hpp"
#include "../include/Axis.hpp"

#include "TCanvas.h"
#include "TDirectory.h"
#include "TF1.h"
#include <memory>
#include <utility>

/**
 * @brief what is a baseline model we can use to compare to our eventual ML model?
 * what's the simplest prediction of BRAN truth from other module truths?
 * maybe E_{BRAN} = a * \sum_{MOD \neq BRAN} E_{MOD} + b? 
 * maybe E_{BRAN} = \sum_{MOD \neq BRAN} a_{MOD} * E_{MOD} + b?
 * BRAN seems to be best correlated with the RPD...
 */

struct SimulationConfig {
  std::string tag;
  std::string path;
  float maxZDCModuleTruthEnergy; // MeV
  float maxBRANTruthEnergy; // MeV
  float maxNonBRANEnergySum; // MeV
  float maxRPDTruthEnergy; // MeV
};

std::array<Color_t, N_SIM_MODULES_USED> constexpr MODULE_COLORS = {
  kRed - 4,
  kOrange + 7,
  kOrange - 2,
  kGreen + 1,
  kAzure + 10,
  kAzure - 3,
};

std::vector<SimulationConfig> const SIM_CONFIGS = {
  {
    "topo_1n_fixedTruth", "../data/zdcTopoAnalysis_1N_fixedTruth.root",
    4e6,
    1e6,
    4e6*2,
    80e3,
  },
  {
    "topo_5n_fixedTruth", "../data/zdcTopoAnalysis_5N_fixedTruth.root",
    8e6,
    2e6,
    8e6*2,
    160e3,
  },
};

std::string const OUT_FILE_PATH = "../plots/predict_bran.root";

void write1D(ROOT::RDF::RResultPtr<TH1D> &hist, bool const logy = true) {
  auto canvas = std::make_unique<TCanvas>(hist->GetName());
  if (logy) {
    gPad->SetLogy();
  }
  hist->Draw("HIST");
  canvas->Write();
}

std::unique_ptr<TCanvas> draw2D(ROOT::RDF::RResultPtr<TH2D> &hist, bool const logz = false, bool const profileX = true) {
  auto canvas = std::make_unique<TCanvas>(hist->GetName());
  if (logz) {
    gPad->SetLogz();
  }
  hist->Draw("COLZ");
  if (profileX) {
    auto *prof = hist->ProfileX();
    prof->SetMarkerStyle(kFullCircle);
    prof->SetMarkerColor(kBlack);
    prof->Draw("SAME");
  }
  return canvas;
}

void write2D(ROOT::RDF::RResultPtr<TH2D> &hist, bool const logz = false, bool const profileX = true) {
  draw2D(hist, logz, profileX)->Write();
}

std::function<float(ROOT::VecOps::RVec<float> const& zdc_ZdcModuleTruthTotal)> getNonBRANSum(unsigned int const side) {
  return [side] (ROOT::VecOps::RVec<float> const& zdc_ZdcModuleTruthTotal) {
    return zdc_ZdcModuleTruthTotal.at(getZDCModuleIndex(side, SimModule::EM))
      + zdc_ZdcModuleTruthTotal.at(getZDCModuleIndex(side, SimModule::HAD1))
      + zdc_ZdcModuleTruthTotal.at(getZDCModuleIndex(side, SimModule::HAD2))
      + zdc_ZdcModuleTruthTotal.at(getZDCModuleIndex(side, SimModule::HAD3))
      + zdc_ZdcModuleTruthTotal.at(getZDCModuleIndex(side, SimModule::RPD));
  };
}

double linear(double const * const x, double const * const p) {
  return p[0]*x[0] + p[1];
}

double specialLinear(double const * const x, double const * const p) {
  if (x[0] == 0) return 0;
  return p[0]*x[0] + p[1];
}

std::function<float(float const RPD_truth)> evalLinear(std::array<double, 2> const& params) {
  return [params] (float const RPD_truth) {
    double const x = RPD_truth;
    return linear(&x, params.data());
  };
}

std::function<float(float const RPD_truth)> evalSpecialLinear(std::array<double, 2> const& params) {
  return [params] (float const RPD_truth) {
    double const x = RPD_truth;
    return specialLinear(&x, params.data());
  };
}

float subtract(float const a, float const b) {
  return a - b;
}

class EvalPredResults {
 public:
  EvalPredResults(
    std::vector<ROOT::RDF::RNode> &unpackedDataframe,
    std::array<std::array<double, 2>, 2> const& linearFitParams,
    std::array<std::array<double, 2>, 2> const& specialLinearFitParams,
    axis::Axis const& truthBRANAxis,
    axis::Axis const& truthRPDAxis,
    axis::Axis const& linearResidualAxis
  ) : m_linearFitParams(linearFitParams),
      m_specialLinearFitParams(specialLinearFitParams),
      m_axes {truthBRANAxis, truthRPDAxis, linearResidualAxis}
  {
    std::vector<ROOT::RDF::RNode> fullyDefinedDataframe;
    for (auto side : SIDES) {
      auto dataframePred = unpackedDataframe.at(side).Define(
        "linearPred", evalLinear(linearFitParams.at(side)), {"RPD_truth"}
      ).Define(
        "linearResidual", subtract, {"linearPred", "BRAN_truth"}
      ).Define(
        "specialLinearPred", evalSpecialLinear(specialLinearFitParams.at(side)), {"RPD_truth"}
      ).Define(
        "specialLinearResidual", subtract, {"specialLinearPred", "BRAN_truth"}
      );
      fullyDefinedDataframe.push_back(dataframePred);
    }
    m_linearResults.emplace("linear", m_axes, fullyDefinedDataframe, "linearPred", "linearResidual");
    m_specialLinearResults.emplace("specialLinear", m_axes, fullyDefinedDataframe, "specialLinearPred", "specialLinearResidual");
  }
  void write(unsigned int const side) {
    m_linearResults.value().write(side);
    m_specialLinearResults.value().write(side);
  }
  struct Axes {
    axis::Axis truthBRANAxis;
    axis::Axis truthRPDAxis;
    axis::Axis linearResidualAxis;
  };
 private:
  class SingleMethodResults {
   public:
    SingleMethodResults(
      std::string method,
      EvalPredResults::Axes const& axes,
      std::vector<ROOT::RDF::RNode> &fullyDefinedDataframe,
      std::string const& predBranch,
      std::string const& residualBranch
    ) : m_method(std::move(method)), m_axes(axes) {
      for (auto const side : SIDES) {
        m_BRANTruthVsPred.at(side) = fullyDefinedDataframe.at(side).Histo2D<float, float>(
          {
            Form("side%c_%s_BRANTruthVsPred", getSideLabel(side), m_method.c_str()),
            ";Actual BRAN Truth Total Energy [MeV];Predicted BRAN Truth Total [MeV];Count",
            BINS(axes.truthBRANAxis), BINS(axes.truthBRANAxis)
          }, "BRAN_truth", predBranch
        );
        m_residual.at(side) = fullyDefinedDataframe.at(side).Histo1D<float>(
          {
            Form("side%c_%s_residual", getSideLabel(side), m_method.c_str()),
            ";Predicted - Actual BRAN Truth Total [MeV];Count",
            BINS(m_axes.linearResidualAxis)
          }, residualBranch
        );
        m_RPDTruthVsResidual.at(side) = fullyDefinedDataframe.at(side).Histo2D<float, float>(
          {
            Form("side%c_%s_RPDTruthVsResidual", getSideLabel(side), m_method.c_str()),
            ";RPD Truth Total Energy [MeV];Predicted - Actual BRAN Truth Total [MeV];Count",
            BINS(axes.truthRPDAxis), BINS(m_axes.linearResidualAxis)
          }, "RPD_truth", residualBranch
        );
        m_BRANTruthVsResidual.at(side) = fullyDefinedDataframe.at(side).Histo2D<float, float>(
          {
            Form("side%c_%s_BRANTruthVsResidual", getSideLabel(side), m_method.c_str()),
            ";BRAN Truth Total Energy [MeV];Predicted - Actual BRAN Truth Total [MeV];Count",
            BINS(axes.truthBRANAxis), BINS(m_axes.linearResidualAxis)
          }, "BRAN_truth", residualBranch
        );
      }
    }
    void write(unsigned int const side) {
      auto *curDir = static_cast<TDirectory*> gDirectory;
      curDir->mkdir(m_method.c_str())->cd();
      write2D(m_BRANTruthVsPred.at(side), true, false);
      write1D(m_residual.at(side), false);
      write2D(m_RPDTruthVsResidual.at(side), true, false);
      write2D(m_BRANTruthVsResidual.at(side), true, false);
      curDir->cd();
    }
   private:
    std::string const m_method;
    EvalPredResults::Axes const& m_axes;
    std::array<ROOT::RDF::RResultPtr<TH2D>, 2> m_BRANTruthVsPred {};
    std::array<ROOT::RDF::RResultPtr<TH1D>, 2> m_residual {};
    std::array<ROOT::RDF::RResultPtr<TH2D>, 2> m_RPDTruthVsResidual {};
    std::array<ROOT::RDF::RResultPtr<TH2D>, 2> m_BRANTruthVsResidual {};
  };
  std::array<std::array<double, 2>, 2> const m_linearFitParams;
  std::array<std::array<double, 2>, 2> const m_specialLinearFitParams;
  Axes const m_axes;
  std::optional<SingleMethodResults> m_linearResults = std::nullopt;
  std::optional<SingleMethodResults> m_specialLinearResults = std::nullopt;
};

void writePlot(SimulationConfig const& config) {
  axis::Axis const truthZDCModuleAxis {256, 0, config.maxZDCModuleTruthEnergy};
  axis::Axis const truthBRANAxis {256, 0, config.maxBRANTruthEnergy};
  axis::Axis const truthNonBRANSumAxis {256, 0, config.maxNonBRANEnergySum};
  axis::Axis const truthRPDAxis {256, 0, config.maxRPDTruthEnergy};
  axis::Axis const linearResidualAxis {256, -config.maxBRANTruthEnergy/2, config.maxBRANTruthEnergy/2};
  auto dataframe = ROOT::RDataFrame("zdcTree", config.path);
  std::array<std::array<ROOT::RDF::RResultPtr<TH1D>, N_SIM_MODULES_USED>, 2> hModuleTruthEnergy {};
  std::array<ROOT::RDF::RResultPtr<TH2D>, 2> hNonBRANSumVsBRAN {};
  std::array<ROOT::RDF::RResultPtr<TH2D>, 2> hRPDVsBRAN {};
  std::vector<ROOT::RDF::RNode> unpackedDataframe;
  for (auto const& side : SIDES) {
    ROOT::RDF::RNode dataframeUnpacked = dataframe;
    // define columns for each of the individual modules
    for (unsigned int mod = 0; mod < N_SIM_MODULES_USED; mod++) {
      dataframeUnpacked = dataframeUnpacked.Define(
        SIM_MODULE_NAMES.at(mod) + "_truth",
        RDFHelp::getZDCModule<float>(side, static_cast<SimModule>(mod)),
        {MODULE_TRUTH_TOTAL_ENERGIES_BRANCH}
      );
    }
    // define column for sum of all non-BRAN modules
    dataframeUnpacked = dataframeUnpacked.Define(
      "nonBRANSum", getNonBRANSum(side), {MODULE_TRUTH_TOTAL_ENERGIES_BRANCH}
    );
    // histogram individual module truth energies
    for (unsigned int mod = 0; mod < N_SIM_MODULES_USED; mod++) {
      hModuleTruthEnergy.at(side).at(mod) = dataframeUnpacked.Histo1D<float>(
        {
          Form("side%c_%s_truth", getSideLabel(side), SIM_MODULE_NAMES.at(mod).c_str()),
          Form(";%s Module Truth Total Energy [MeV];Count", SIM_MODULE_NAMES.at(mod).c_str()),
          BINS(truthZDCModuleAxis)
        },
        SIM_MODULE_NAMES.at(mod) + "_truth"
      );
    }
    // histogram non-BRAN truth energy sum vs BRAN truth energy
    hNonBRANSumVsBRAN.at(side) = dataframeUnpacked.Histo2D<float, float>(
      {
        Form("side%c_nonBRANSumVsBRAN", getSideLabel(side)),
        ";Non-BRAN Module Truth Total Energy Sum [MeV];BRAN Truth Total Energy [MeV];Count",
        BINS(truthNonBRANSumAxis), BINS(truthBRANAxis)
      }, "nonBRANSum", "BRAN_truth"
    );
    // histogram RPD vs BRAN truth energy
    hRPDVsBRAN.at(side) = dataframeUnpacked.Histo2D<float, float>(
      {
        Form("side%c_RPDVsBRAN", getSideLabel(side)),
        ";RPD Truth Total Energy [MeV];BRAN Truth Total Energy [MeV];Count",
        BINS(truthRPDAxis), BINS(truthBRANAxis)
      }, "RPD_truth", "BRAN_truth"
    );
    unpackedDataframe.push_back(dataframeUnpacked);
  }

  // now do some analysis using the results
  std::array<std::array<double, 2>, 2> linearFitParams {};
  std::array<std::array<double, 2>, 2> specialLinearFitParams {};
  auto funcLinear = std::make_unique<TF1>("linear", linear, 0, 1, 2);
  funcLinear->SetLineColor(kRed);
  funcLinear->SetNpx(truthRPDAxis.nbins);
  auto funcSpecialLinear = std::make_unique<TF1>("specialLinear", specialLinear, 0, 1, 2);
  funcSpecialLinear->SetLineColor(kGreen);
  funcSpecialLinear->SetNpx(truthRPDAxis.nbins - 1);
  for (auto const side : SIDES) {
    // fit linear function
    funcLinear->SetRange(hRPDVsBRAN.at(side)->GetXaxis()->GetXmin(), hRPDVsBRAN.at(side)->GetXaxis()->GetXmax());
    hRPDVsBRAN.at(side)->Fit(funcLinear.get(), "QR+");
    linearFitParams.at(side).at(0) = funcLinear->GetParameter(0);
    linearFitParams.at(side).at(1) = funcLinear->GetParameter(1);
    // fit special linear function, ignoring first bin since we map zero too zero!
    funcSpecialLinear->SetRange(hRPDVsBRAN.at(side)->GetXaxis()->GetBinUpEdge(1), hRPDVsBRAN.at(side)->GetXaxis()->GetXmax());
    hRPDVsBRAN.at(side)->Fit(funcSpecialLinear.get(), "QR+");
    specialLinearFitParams.at(side).at(0) = funcSpecialLinear->GetParameter(0);
    specialLinearFitParams.at(side).at(1) = funcSpecialLinear->GetParameter(1);
  }
  // use fits to predict BRAN energy and find error
  EvalPredResults evalPredResults(
    unpackedDataframe,
    linearFitParams,
    specialLinearFitParams,
    truthBRANAxis,
    truthRPDAxis,
    linearResidualAxis
  );

  for (auto const side : SIDES) {
    auto *curDir = static_cast<TDirectory*> gDirectory;
    curDir->mkdir(Form("side%c", getSideLabel(side)))->cd();
    for (unsigned int i = 0; i < N_SIM_MODULES_USED; i++) {
      auto const mod = MODULE_PLOT_ORDER.at(i);
      auto & hist = hModuleTruthEnergy.at(side).at(mod);
      write1D(hist);
    }
    write2D(hNonBRANSumVsBRAN.at(side), true);
    write2D(hRPDVsBRAN.at(side), true);
    evalPredResults.write(side);
    curDir->cd();
  }
}

void predict_bran() {
  ROOT::EnableImplicitMT();
  TH1::SetDefaultSumw2(true); // needed since we're fitting histograms...?
  TFile *outFile = TFile::Open(OUT_FILE_PATH.c_str(), "RECREATE");
  for (auto const& config : SIM_CONFIGS) {
    outFile->mkdir(config.tag.c_str())->cd();
    writePlot(config);
  }
  outFile->Close();
}
